# Instrukcje do projektu

## Preferowany IDE
- IntelliJ


## Instrukcja uruchomienia bazy
1. Otwórz dwa okna cmd w folderze: `\hsqldb-2.7.3\hsqldb`
2. W pierwszym oknie cmd uruchom poniższe polecenie:
    ```sh
    java -classpath lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/hemrajdb --dbname.0 testdb
    ```
3. W drugim oknie cmd uruchom poniższe polecenie:
    ```sh
    java -classpath lib/hsqldb.jar org.hsqldb.util.DatabaseManagerSwing
    ```
4. Powinno wyświetlić się okienko. Wprowadź dane:
![Dane do bazy](https://gitlab.com/gzim331/lab5.2-twarza_po_asfalcie/-/raw/master/src/main/resources/img/daneDoBazy.png)
5. Skrypty do bazy danych znajdują się w folderze: `\src\main\resources\sql`


## Klasa testowa
Plik klasy testowej znajduje się w: `src\main\java\pl\wwsis\sos\App.java`


## Reset bazy
Przed uruchomieniem bazy zaleca się usunięcie plików `.properties` oraz `.script` w folderze: `/hsqldb-2.7.3/hsqldb/hsqldb`