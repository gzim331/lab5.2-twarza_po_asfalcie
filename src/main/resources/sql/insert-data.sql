-- TABELA: autor
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Marcin', 'Lemiesz', 'twórca AI');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Michał', 'Zimka', 'zimny drań');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Zbyszek', 'Niełacny', 'jego już nie ma');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Tobiasz', 'Cetera', 'nieposkromiony ogier');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Andrzej', 'Kowalski', 'Ekspert od fizyki');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Barbara', 'Nowak', 'Wybitna matematyczka');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Celina', 'Wiśniewska', 'Specjalistka od biologii');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Daniel', 'Wójcik', 'Geniusz informatyki');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Ewa', 'Kozłowska', 'Znawczyni języków programowania');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Franciszek', 'Kamiński', 'Pasjonat fizyki teoretycznej');
INSERT INTO autor (imie, nazwisko, biografia)
VALUES ('Grażyna', 'Zielińska', 'Ekspertka od matematyki stosowanej');


-- TABELA: ksiazka
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Tobiasz w krainie czarów', 2, '2024-05-27', 'HELION', 2, 'Szmatławiec',
        (SELECT id FROM autor WHERE nazwisko = 'Zimka' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Mama i ja', 1, '2020-01-27', 'HELION', 2, 'Megahit',
        (SELECT id FROM autor WHERE nazwisko = 'Cetera' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Mordą po asfalcie', 1, '1997-01-27', 'HELION', 2, 'Szmatławiec',
        (SELECT id FROM autor WHERE imie = 'Zbyszek' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Podstawy fizyki', 300, '2015-03-15', 'PWN', 1, 'Fizyka',
        (SELECT id FROM autor WHERE nazwisko = 'Kowalski' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Zaawansowana matematyka', 400, '2018-06-20', 'PWN', 2, 'Matematyka',
        (SELECT id FROM autor WHERE nazwisko = 'Nowak' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Biologia komórki', 350, '2020-09-10', 'PWN', 1, 'Biologia',
        (SELECT id FROM autor WHERE nazwisko = 'Wiśniewska' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Algorytmy i struktury danych', 250, '2021-12-05', 'PWN', 1, 'Informatyka',
        (SELECT id FROM autor WHERE nazwisko = 'Wójcik' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Programowanie w Pythonie', 200, '2019-11-11', 'PWN', 1, 'Języki programowania',
        (SELECT id FROM autor WHERE nazwisko = 'Kozłowska' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Fizyka teoretyczna', 450, '2017-04-18', 'PWN', 1, 'Fizyka',
        (SELECT id FROM autor WHERE nazwisko = 'Kamiński' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Matematyka stosowana', 380, '2016-05-22', 'PWN', 1, 'Matematyka',
        (SELECT id FROM autor WHERE nazwisko = 'Zielińska' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Biochemia', 320, '2019-08-15', 'PWN', 1, 'Biologia',
        (SELECT id FROM autor WHERE nazwisko = 'Wiśniewska' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Zaawansowane algorytmy', 290, '2022-03-30', 'PWN', 1, 'Informatyka',
        (SELECT id FROM autor WHERE nazwisko = 'Wójcik' LIMIT 1));
INSERT INTO ksiazka (tytul, ilosc_stron, data_wydania, wydawnictwo, wydanie, kategoria, autor_id)
VALUES ('Programowanie w Javie', 240, '2021-07-07', 'PWN', 1, 'Języki programowania',
        (SELECT id FROM autor WHERE nazwisko = 'Kozłowska' LIMIT 1));


-- TABELA: egzemplarz
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0001', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Podstawy fizyki' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0002', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Podstawy fizyki' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0003', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Podstawy fizyki' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0004', 'ZAREZERWOWANY',
        (SELECT id FROM ksiazka WHERE tytul = 'Zaawansowana matematyka' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0005', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Zaawansowana matematyka' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0006', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Zaawansowana matematyka' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0007', 'ZAREZERWOWANY', (SELECT id FROM ksiazka WHERE tytul = 'Biologia komórki' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0008', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Biologia komórki' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0009', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Biologia komórki' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0010', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Algorytmy i struktury danych' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0012', 'WYPOZYCZONY',
        (SELECT id FROM ksiazka WHERE tytul = 'Algorytmy i struktury danych' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0013', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Algorytmy i struktury danych' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0012', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Programowanie w Pythonie' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0013', 'WYPOZYCZONY',
        (SELECT id FROM ksiazka WHERE tytul = 'Programowanie w Pythonie' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0014', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Programowanie w Pythonie' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0015', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Fizyka teoretyczna' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0016', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Fizyka teoretyczna' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0017', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Fizyka teoretyczna' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0018', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Matematyka stosowana' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0019', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Matematyka stosowana' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0020', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Matematyka stosowana' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0021', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Biochemia' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0022', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Biochemia' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0023', 'DOSTEPNY', (SELECT id FROM ksiazka WHERE tytul = 'Biochemia' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0024', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Zaawansowane algorytmy' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0025', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Zaawansowane algorytmy' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0026', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Zaawansowane algorytmy' LIMIT 1));


INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0027', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Programowanie w Javie' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0028', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Programowanie w Javie' LIMIT 1));
INSERT INTO egzemplarz (id_egzemplarza, stan, ksiazka_id)
VALUES ('0029', 'DOSTEPNY',
        (SELECT id FROM ksiazka WHERE tytul = 'Programowanie w Javie' LIMIT 1));


-- TABELA: adres
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Warszawa', 'Aleje Jerozolimskie', '10', '5');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Kraków', 'ul. Krakowska', '20', '8');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Gdańsk', 'ul. Gdańska', '30', '12');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Poznań', 'ul. Poznańska', '40', '15');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Wrocław', 'ul. Wrocławska', '50', '20');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Łódź', 'ul. Łódzka', '60', '25');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Szczecin', 'ul. Szczecińska', '70', '30');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Katowice', 'ul. Katowicka', '80', '35');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Gdynia', 'ul. Gdyńska', '90', '40');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Bydgoszcz', 'ul. Bydgoska', '100', '45');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Lublin', 'ul. Lubelska', '110', '50');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Białystok', 'ul. Białostocka', '120', '55');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Kielce', 'ul. Kielecka', '130', '60');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Olsztyn', 'ul. Olsztyńska', '140', '65');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Rzeszów', 'ul. Rzeszowska', '150', '70');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Toruń', 'ul. Toruńska', '160', '75');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Opole', 'ul. Opolska', '170', '80');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Zielona Góra', 'ul. Zielonogórska', '180', '85');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Gliwice', 'ul. Gliwicka', '190', '90');
INSERT INTO adres (kraj, miasto, ulica, nr_domu, nr_mieszkania)
VALUES ('Polska', 'Słupsk', 'ul. Słupska', '200', '95');


-- TABELA: osoba
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Radosław', 'Pawlak', '67890123456', 0, '678-901-234', 'radoslaw.pawlak@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Marta', 'Lis', '67890223468', 1, '678-901-234', 'marta.lis@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Michał', 'Nowicki', '78901234567', 2, '789-012-345', 'michal.nowicki@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Magdalena', 'Wozniak', '89012345678', 3, '890-123-456', 'magdalena.wozniak@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Krzysztof', 'Jankowski', '90123456789', 4, '901-234-567', 'krzysztof.Jankowski@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Alicja', 'Duda', '01234567890', 5, '012-345-678', 'alicja.duda@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Robert', 'Kamiński', '23456789012', 6, '234-567-890', 'robert.kaminski@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Ewa', 'Zając', '34567890123', 7, '345-678-901', 'ewa.zajac@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Katarzyna', 'Szymańska', '45678901234', 8, '456-789-012', 'katarzyna.szymanska@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Wojciech', 'Wojciechowski', '56789012345', 9, '567-890-123', 'wojciech.wojciechowski@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Karolina', 'Krawczyk', '67890123457', 10, '678-901-234', 'karolina.krawczyk@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Rafał', 'Piotrowski', '78901234568', 11, '789-012-345', 'rafal.piotrowski@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Natalia', 'Grabowska', '89012345679', 12, '890-123-456', 'natalia.grabowska@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Łukasz', 'Nowakowski', '90123456780', 13, '901-234-567', 'lukasz.nowakowski@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Patrycja', 'Dąbrowska', '01234567891', 14, '012-345-678', 'patrycja.dabrowska@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Grzegorz', 'Kowalczyk', '12345678901', 15, '123-456-789', 'grzegorz.kowalczyk@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Monika', 'Lewandowska', '23456789013', 16, '234-567-890', 'monika.lewandowska@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Mateusz', 'Wójcik', '34567890124', 17, '345-678-901', 'mateusz.wojcik@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Karol', 'Kaczmarek', '45678901235', 18, '456-789-012', 'karol.kaczmarek@example.com');
INSERT INTO osoba (imie, nazwisko, pesel, adres_id, telefon, email)
VALUES ('Marcelina', 'Zielińska', '56789012346', 19, '567-890-123', 'marcelina.zielinska@example.com');


-- TABELA: konto
INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('rpawlak', 'password123', true, 'WYKLADOWCA',
        (SELECT id FROM osoba WHERE pesel = '67890123456')); -- 0


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('mlis', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '67890223468')); -- 1


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('mnowicki', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '78901234567')); -- 2


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('mwozniak', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '89012345678')); -- 3


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('kjankowski', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '90123456789')); -- 4


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('aduda', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '01234567890')); -- 5


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('rkaminski', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '23456789012')); -- 6


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('ezajac', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '34567890123')); -- 7


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('kszymanska', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '45678901234')); -- 8


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('wwojciechowski', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '56789012345')); -- 9


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('kkrawczyk', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '67890123457')); -- 10


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('rpiotrowski', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '78901234568')); -- 11


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('ngrabowska', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '89012345679')); -- 12


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('lnowakowski', 'password123', true, 'STUDENT',
        (SELECT id FROM osoba WHERE pesel = '90123456780')); -- 13


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('pdabrowska', 'password123', true, 'KSIEGOWY',
        (SELECT id FROM osoba WHERE pesel = '01234567891')); -- 14


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('gkowalczyk', 'password123', true, 'BIBLIOTEKARZ',
        (SELECT id FROM osoba WHERE pesel = '12345678901')); -- 15


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('mlewandowska', 'password123', true, 'WYKLADOWCA',
        (SELECT id FROM osoba WHERE pesel = '23456789013')); -- 16


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('mwojcik', 'password123', true, 'WYKLADOWCA',
        (SELECT id FROM osoba WHERE pesel = '34567890124')); -- 17


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('kkaczmarek', 'password123', true, 'WYKLADOWCA',
        (SELECT id FROM osoba WHERE pesel = '45678901235')); -- 18


INSERT INTO konto (login, haslo, status, rola, osoba_id)
VALUES ('mzielinska', 'password123', true, 'WYKLADOWCA',
        (SELECT id FROM osoba WHERE pesel = '56789012346'));
-- 19


-- TABELA: grupa
INSERT INTO grupa (nazwa_kierunku, semestr)
VALUES ('INFORMATYKA', 1);
INSERT INTO grupa (nazwa_kierunku, semestr)
VALUES ('AUTOMATYKA_I_ROBOTYKA', 3);
INSERT INTO grupa (nazwa_kierunku, semestr)
VALUES ('MECHATRONIKA', 5);


-- TABELA: student
INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2022-10-01', false, 'S001', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '67890123456'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'INFORMATYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2021-10-01', true, 'S002', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '67890223468'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'INFORMATYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2020-10-01', false, 'S003', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '78901234567'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'AUTOMATYKA_I_ROBOTYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2019-10-01', true, 'S004', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '89012345678'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'AUTOMATYKA_I_ROBOTYKA'));

INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2022-10-01', false, 'S005', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '90123456789'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'MECHATRONIKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2021-10-01', true, 'S006', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '01234567890'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'MECHATRONIKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2020-10-01', false, 'S007', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '23456789012'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'INFORMATYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2019-10-01', true, 'S008', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '34567890123'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'INFORMATYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2022-10-01', false, 'S009', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '45678901234'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'AUTOMATYKA_I_ROBOTYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2021-10-01', true, 'S010', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '56789012345'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'AUTOMATYKA_I_ROBOTYKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2020-10-01', false, 'S011', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '67890123457'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'MECHATRONIKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2019-10-01', true, 'S012', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '78901234568'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'MECHATRONIKA'));


INSERT INTO student (data_rozpoczęcia_studiow, tok_indywidualny, numer_umowy, status, osoba_id, grupa_id)
VALUES ('2022-10-01', false, 'S013', 'aktywny',
        (SELECT id FROM osoba WHERE pesel = '89012345679'),
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'INFORMATYKA'));


-- TABELA: pracownik
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Dziekan', '2010-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '90123456780'));
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Prodziekan', '2012-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '01234567891'));
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Sekretarz', '2015-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '12345678901'));
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Asystent', '2018-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '23456789013'));
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Profesor', '2020-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '34567890124'));
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Bibliotekarz', '2021-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '45678901235'));
INSERT INTO pracownik (stanowisko, data_zatrudnienia, data_zakonczenia_pracy, osoba_id)
VALUES ('Księgowy', '2021-01-01', NULL,
        (SELECT id FROM osoba WHERE pesel = '56789012346'));


-- TABELA: ksiegowy
INSERT INTO ksiegowy (dzial, pracownik_id)
VALUES ('Biblioteka Uniwersytecka',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '90123456780'));
-- 0


-- TABELA: bibliotekarz
INSERT INTO bibliotekarz (ranga, pracownik_id)
VALUES ('KIEROWNIK',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '01234567891'));
-- 1

-- TABELA: wykladowca
INSERT INTO wykladowca (stopien_naukowy, specjalizacja, doswiadczenie, pracownik_id)
VALUES ('DOKTOR', 'Informatyka', 'Od 2010-01-01',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '12345678901')); -- 2


INSERT INTO wykladowca (stopien_naukowy, specjalizacja, doswiadczenie, pracownik_id)
VALUES ('DOKTOR', 'Matematyka', 'Od 2012-01-01',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '23456789013')); -- 3


INSERT INTO wykladowca (stopien_naukowy, specjalizacja, doswiadczenie, pracownik_id)
VALUES ('MAGISTER', 'Fizyka', 'Od 2015-01-01',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '34567890124')); -- 4


INSERT INTO wykladowca (stopien_naukowy, specjalizacja, doswiadczenie, pracownik_id)
VALUES ('DOKTOR', 'Chemia', 'Od 2018-01-01',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '45678901235')); -- 5


INSERT INTO wykladowca (stopien_naukowy, specjalizacja, doswiadczenie, pracownik_id)
VALUES ('PROFESOR', 'Biologia', 'Od 2020-01-01',
        (SELECT p.id
         FROM pracownik p
                  JOIN osoba o ON p.osoba_id = o.id
         WHERE pesel = '56789012346'));
-- 6


-- TABELA: wypozyczenie
INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890123456')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0001'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890123456')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0002'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890123456')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0003'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0004'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0005'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0006'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '78901234567')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0007'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '78901234567')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0008'));

INSERT INTO wypozyczenie (data_wypozyczenia, data_rezerwacji, data_oddania, termin_oddania, bibliotekarz_id, student_id,
                          egzemplarz_id)
VALUES ('2024-05-01', '2024-02-25', NULL, '2024-04-01',
        (SELECT id
         FROM bibliotekarz
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567891'))),
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '78901234567')),
        (SELECT id FROM egzemplarz WHERE id_egzemplarza = '0009'));


-- TABELA: przedmiot
INSERT INTO przedmiot (nazwa_przedmiotu, punkty_ects, liczba_godzin, jezyk_wykladowy, opis, kierunek_studiow,
                       semestr, tryb_studiow, wykladowca_id)
VALUES ('Matematyka Dyskretna', 6, 60, 'PL', 'Podstawy matematyki dyskretnej', 'INFORMATYKA', 1, 'STACJONARNE',
        (SELECT id
         FROM wykladowca
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '12345678901'))));

INSERT INTO przedmiot (nazwa_przedmiotu, punkty_ects, liczba_godzin, jezyk_wykladowy, opis, kierunek_studiow,
                       semestr, tryb_studiow, wykladowca_id)
VALUES ('Algorytmy i Struktury Danych', 5, 50, 'PL', 'Podstawy algorytmów i struktur danych', 'INFORMATYKA', 1,
        'STACJONARNE',
        (SELECT id
         FROM wykladowca
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '23456789013'))));

INSERT INTO przedmiot (nazwa_przedmiotu, punkty_ects, liczba_godzin, jezyk_wykladowy, opis, kierunek_studiow,
                       semestr, tryb_studiow, wykladowca_id)
VALUES ('Fizyka Kwantowa', 6, 60, 'PL', 'Wprowadzenie do fizyki kwantowej', 'MECHATRONIKA', 2, 'STACJONARNE',
        (SELECT id
         FROM wykladowca
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '34567890124'))));

-- TABELA: harmonogram
INSERT INTO harmonogram (data_zajec, start_zajec, koniec_zajec, grupa_id, przedmiot_id)
VALUES ('2024-09-10', '08:00:00', '10:00:00',
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'INFORMATYKA' AND semestr = 1),
        (SELECT id FROM przedmiot WHERE nazwa_przedmiotu = 'Matematyka Dyskretna'));

INSERT INTO harmonogram (data_zajec, start_zajec, koniec_zajec, grupa_id, przedmiot_id)
VALUES ('2024-09-11', '10:00:00', '12:00:00',
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'AUTOMATYKA_I_ROBOTYKA' AND semestr = 3),
        (SELECT id FROM przedmiot WHERE nazwa_przedmiotu = 'Algorytmy i Struktury Danych'));

INSERT INTO harmonogram (data_zajec, start_zajec, koniec_zajec, grupa_id, przedmiot_id)
VALUES ('2024-09-12', '08:00:00', '10:00:00',
        (SELECT id FROM grupa WHERE nazwa_kierunku = 'MECHATRONIKA' AND semestr = 5),
        (SELECT id FROM przedmiot WHERE nazwa_przedmiotu = 'Fizyka Kwantowa'));


--- TUTAJ
-- TABELA: protokol
INSERT INTO protokol (nr_protokolu, ocena, status, data_zamkniecia, data_oceny, przedmiot_id, wykladowca_id)
VALUES (101, 5.0, true, '2024-01-10', '2024-01-05',
        (SELECT id FROM przedmiot WHERE nazwa_przedmiotu = 'Matematyka Dyskretna'),
        (SELECT id
         FROM wykladowca
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '12345678901'))));

INSERT INTO protokol (nr_protokolu, ocena, status, data_zamkniecia, data_oceny, przedmiot_id, wykladowca_id)
VALUES (203, 4.5, true, '2024-01-12', '2024-01-08',
        (SELECT id FROM przedmiot WHERE nazwa_przedmiotu = 'Algorytmy i Struktury Danych'),
        (SELECT id
         FROM wykladowca
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '23456789013'))));


INSERT INTO protokol (nr_protokolu, ocena, status, data_zamkniecia, data_oceny, przedmiot_id, wykladowca_id)
VALUES (102, 4.0, true, '2024-01-15', '2024-01-10',
        (SELECT id FROM przedmiot WHERE nazwa_przedmiotu = 'Fizyka Kwantowa'),
        (SELECT id
         FROM wykladowca
         WHERE pracownik_id =
               (SELECT id FROM pracownik WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '34567890124'))));

-- TABELA: indeks_elektroniczny_studenta
-- Marta Lis
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (1, '2022-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '67890123456'));

-- Michał Nowicki
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (2, '2021-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '67890223468'));

-- Magdalena Wozniak
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (3, '2020-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '78901234567'));

-- Krzysztof Jankowski
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (4, '2019-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '89012345678'));

-- Alicja Duda
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (5, '2022-10-01', 'Nadprzewodniki wysokotemperaturowe - właściwości', NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '90123456789'));

-- Robert Kamiński
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (6, '2021-10-01', 'Symulacje dynamiki cząstek w plazmie', NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '01234567890'));

-- Ewa Zając
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (7, '2020-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '23456789012'));

-- Katarzyna Szymańska
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (8, '2019-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '34567890123'));

-- Wojciech Wojciechowski
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (9, '2022-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '45678901234'));

-- Karolina Krawczyk
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (10, '2021-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '56789012345'));

-- Rafał Piotrowski
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (11, '2020-10-01', 'Teoria chaosu w analizie systemów fizycznych', NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '67890123457'));

-- Natalia Grabowska
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (12, '2019-10-01', 'Fotowoltaika w materiałach półprzewodnikowych', NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '78901234568'));

-- Łukasz Nowakowski
INSERT INTO indeks_elektroniczny_studenta (nr_indeksu, data_zalozenia, temat_pracy_dyplomowej, zdjecie, student_id)
VALUES (13, '2022-10-01', NULL, NULL,
        (SELECT s.id
         FROM student s
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '89012345679'));


-- TABELA: wpis_do_indeksu
INSERT INTO wpis_do_indeksu (data_wpis, ocena, protokol_id, indeks_elektroniczny_studenta_id)
VALUES ('2024-01-11', 5.0,
        (SELECT id FROM protokol WHERE nr_protokolu = 101),
        (SELECT id
         FROM indeks_elektroniczny_studenta ies
                  JOIN student s ON ies.student_id = s.id
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE pesel = '67890123456'));

INSERT INTO wpis_do_indeksu (data_wpis, ocena, protokol_id, indeks_elektroniczny_studenta_id)
VALUES ('2024-01-09', 4.5,
        (SELECT id FROM protokol WHERE nr_protokolu = 203),
        (SELECT ies.id
         FROM indeks_elektroniczny_studenta ies
                  JOIN student s ON ies.student_id = s.id
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '67890223468'));

INSERT INTO wpis_do_indeksu (data_wpis, ocena, protokol_id, indeks_elektroniczny_studenta_id)
VALUES ('2024-01-12', 4.0,
        (SELECT id FROM protokol WHERE nr_protokolu = 102),
        (SELECT ies.id
         FROM indeks_elektroniczny_studenta ies
                  JOIN student s ON ies.student_id = s.id
                  JOIN osoba o ON s.osoba_id = o.id
         WHERE o.pesel = '78901234567'));


-- TABELA: legitymacja
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1001', '2024-10-31', 'zdjecie1.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890123456')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1002', '2025-10-31', 'zdjecie2.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1003', '2022-10-31', 'zdjecie3.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '78901234567')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1004', '2022-10-31', 'zdjecie4.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '89012345678')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1005', '2022-10-31', 'zdjecie5.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '90123456789')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1006', '2022-10-31', 'zdjecie6.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '01234567890')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1007', '2022-10-31', 'zdjecie7.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '23456789012')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1008', '2022-10-31', 'zdjecie8.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '34567890123')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('1009', '2022-10-31', 'zdjecie9.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '45678901234')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('10010', '2022-10-31', 'zdjecie10.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '56789012345')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('10011', '2022-10-31', 'zdjecie11.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890123457')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('10012', '2022-10-31', 'zdjecie12.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '78901234568')));
INSERT INTO legitymacja_studenta (numer_legitymacji, data_waznosci, zdjecie, student_id)
VALUES ('10013', '2022-10-31', 'zdjecie13.png',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '89012345679')));


-- TABELA: operacja_finansowa
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-10-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-09-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-08-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-07-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-06-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));
INSERT INTO operacja_finansowa (utworzono, kwota, kierunek, tytuł, typ_transakcji, student_id)
VALUES ('2022-05-12', 695, 'INCOMMING', 'oplata za studia', 'czesne',
        (SELECT id FROM student WHERE osoba_id = (SELECT id FROM osoba WHERE pesel = '67890223468')));

INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S001'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Matematyka Dyskretna'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S001'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Algorytmy i Struktury Danych'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S001'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Fizyka Kwantowa'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S002'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Matematyka Dyskretna'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S002'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Algorytmy i Struktury Danych'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S002'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Fizyka Kwantowa'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S003'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Matematyka Dyskretna'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S003'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Algorytmy i Struktury Danych'));
INSERT INTO student_przedmiot (student_id, przedmiot_id)
VALUES ((SELECT id FROM student WHERE NUMER_UMOWY = 'S003'),
        (SELECT id FROM przedmiot WHERE NAZWA_PRZEDMIOTU = 'Fizyka Kwantowa'));
