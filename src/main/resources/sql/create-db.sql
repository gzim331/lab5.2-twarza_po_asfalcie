DROP TABLE wpis_do_indeksu IF EXISTS;
DROP TABLE protokol IF EXISTS;
DROP TABLE harmonogram IF EXISTS;
DROP TABLE przedmiot IF EXISTS;
DROP TABLE wykladowca IF EXISTS;
DROP TABLE indeks_elektroniczny_studenta IF EXISTS;
DROP TABLE wypozyczenie IF EXISTS;
DROP TABLE egzemplarz IF EXISTS;
DROP TABLE ksiazka IF EXISTS;
DROP TABLE autor IF EXISTS;
DROP TABLE bibliotekarz IF EXISTS;
DROP TABLE legitymacja_studenta IF EXISTS;
DROP TABLE operacja_finansowa IF EXISTS;
DROP TABLE ksiegowy IF EXISTS;
DROP TABLE pracownik IF EXISTS;
DROP TABLE student_przedmiot IF EXISTS;
DROP TABLE student IF EXISTS;
DROP TABLE grupa IF EXISTS;
DROP TABLE konto IF EXISTS;
DROP TABLE osoba IF EXISTS;
DROP TABLE adres IF EXISTS;
DROP SCHEMA PUBLIC;

CREATE TABLE IF NOT EXISTS adres
(
    id            integer      NOT NULL PRIMARY KEY IDENTITY,
    kraj          varchar(100) NOT NULL,
    miasto        varchar(100) NOT NULL,
    ulica         varchar(50),
    nr_domu       varchar(5)   NOT NULL,
    nr_mieszkania varchar(5)
);

CREATE TABLE IF NOT EXISTS osoba
(
    id       integer      NOT NULL PRIMARY KEY IDENTITY,
    imie     varchar(100) NOT NULL,
    nazwisko varchar(100) NOT NULL,
    pesel    varchar(15)  NOT NULL,
    telefon  varchar(15)  NOT NULL,
    email    varchar(50)  NOT NULL,
    adres_id integer      NOT NULL,
    FOREIGN KEY (adres_id) REFERENCES adres (id)
);

CREATE TABLE IF NOT EXISTS konto
(
    id       integer              NOT NULL PRIMARY KEY IDENTITY,
    login    varchar(100)         NOT NULL,
    haslo    varchar(100)         NOT NULL,
    status   boolean DEFAULT true NOT NULL,
    rola     varchar(100)         NOT NULL,
    osoba_id integer              NOT NULL,
    FOREIGN KEY (osoba_id) REFERENCES osoba (id)
);

CREATE TABLE IF NOT EXISTS grupa
(
    id             integer     NOT NULL PRIMARY KEY IDENTITY,
    nazwa_kierunku varchar(50) NOT NULL,
    semestr        integer     NOT NULL
);

CREATE TABLE IF NOT EXISTS student
(
    id                       integer NOT NULL PRIMARY KEY IDENTITY,
    data_rozpoczęcia_studiow date    NOT NULL,
    tok_indywidualny         boolean DEFAULT false,
    numer_umowy              varchar(50),
    status                   varchar(50),
    osoba_id                 integer NOT NULL,
    grupa_id                 integer NOT NULL,
    FOREIGN KEY (osoba_id) REFERENCES osoba (id),
    FOREIGN KEY (grupa_id) REFERENCES grupa (id)
);

CREATE TABLE IF NOT EXISTS pracownik
(
    id                     integer     NOT NULL PRIMARY KEY IDENTITY,
    stanowisko             varchar(50) NOT NULL,
    data_zatrudnienia      date        NOT NULL,
    data_zakonczenia_pracy date,
    osoba_id               integer     NOT NULL,
    FOREIGN KEY (osoba_id) REFERENCES osoba (id)
);

CREATE TABLE IF NOT EXISTS ksiegowy
(
    id           integer NOT NULL PRIMARY KEY IDENTITY,
    dzial        varchar(50),
    pracownik_id integer NOT NULL,
    FOREIGN KEY (pracownik_id) REFERENCES pracownik (id)
);

CREATE TABLE IF NOT EXISTS operacja_finansowa
(
    id             integer      NOT NULL PRIMARY KEY IDENTITY,
    utworzono      date         NOT NULL,
    kwota          decimal DEFAULT 0,
    kierunek       varchar(100) NOT NULL,
    tytuł          varchar(100) NOT NULL,
    typ_transakcji varchar(50)  NOT NULL,
    student_id     integer      NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (id)
);

CREATE TABLE IF NOT EXISTS legitymacja_studenta
(
    id                integer     NOT NULL PRIMARY KEY IDENTITY,
    numer_legitymacji varchar(50) NOT NULL,
    data_waznosci     date        NOT NULL,
    zdjecie           varchar(250),
    student_id        integer     NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (id)
);

CREATE TABLE IF NOT EXISTS bibliotekarz
(
    id           integer     NOT NULL PRIMARY KEY IDENTITY,
    ranga        varchar(50) NOT NULL,
    pracownik_id integer     NOT NULL,
    FOREIGN KEY (pracownik_id) REFERENCES pracownik (id)
);

CREATE TABLE IF NOT EXISTS autor
(
    id        integer     NOT NULL PRIMARY KEY IDENTITY,
    imie      varchar(50) NOT NULL,
    nazwisko  varchar(50) NOT NULL,
    biografia VARCHAR(250)
);

CREATE TABLE IF NOT EXISTS ksiazka
(
    id           integer      NOT NULL PRIMARY KEY IDENTITY,
    tytul        varchar(100) NOT NULL,
    ilosc_stron  integer,
    data_wydania date,
    wydawnictwo  varchar(250),
    wydanie      integer,
    kategoria    varchar(100),
    autor_id     integer      NOT NULL,
    FOREIGN KEY (autor_id) REFERENCES autor (id)
);

CREATE TABLE IF NOT EXISTS egzemplarz
(
    id             integer NOT NULL PRIMARY KEY IDENTITY,
    id_egzemplarza varchar(100),
    stan           varchar(50),
    ksiazka_id     integer NOT NULL,
    FOREIGN KEY (ksiazka_id) REFERENCES ksiazka (id)
);

CREATE TABLE IF NOT EXISTS wypozyczenie
(
    id                integer NOT NULL PRIMARY KEY IDENTITY,
    data_wypozyczenia date,
    data_rezerwacji   date,
    data_oddania      date,
    termin_oddania    date,
    bibliotekarz_id   integer NOT NULL,
    student_id        integer NOT NULL,
    egzemplarz_id     integer NOT NULL,
    FOREIGN KEY (bibliotekarz_id) REFERENCES bibliotekarz (id),
    FOREIGN KEY (student_id) REFERENCES student (id),
    FOREIGN KEY (egzemplarz_id) REFERENCES egzemplarz (id)
);

CREATE TABLE IF NOT EXISTS indeks_elektroniczny_studenta
(
    id                     integer NOT NULL PRIMARY KEY IDENTITY,
    nr_indeksu             integer NOT NULL,
    data_zalozenia         date    NOT NULL,
    temat_pracy_dyplomowej varchar(50),
    zdjecie                varchar(200),
    student_id             integer NOT NULL,
    FOREIGN KEY (student_id) REFERENCES indeks_elektroniczny_studenta (id)
);

CREATE TABLE IF NOT EXISTS wykladowca
(
    id              integer                         NOT NULL PRIMARY KEY IDENTITY,
    specjalizacja   varchar(200)                    NOT NULL,
    doswiadczenie   varchar(200)                    NOT NULL,
    stopien_naukowy varchar(200) DEFAULT 'MAGISTER' NOT NULL,
    pracownik_id    integer                         NOT NULL,
    FOREIGN KEY (pracownik_id) REFERENCES pracownik (id)
);

CREATE TABLE IF NOT EXISTS przedmiot
(
    id               integer                            NOT NULL PRIMARY KEY IDENTITY,
    nazwa_przedmiotu varchar(200),
    punkty_ects      integer,
    liczba_godzin    integer                            NOT NULL,
    jezyk_wykladowy  varchar(2)                         NOT NULL,
    opis             varchar(200)                       NOT NULL,
    kierunek_studiow varchar(200)                       NOT NULL,
    semestr          integer                            NOT NULL,
    tryb_studiow     varchar(200) DEFAULT 'STACJONARNE' NOT NULL,
    wykladowca_id    integer                            NOT NULL,
    FOREIGN KEY (wykladowca_id) REFERENCES wykladowca (id)
);

CREATE TABLE IF NOT EXISTS harmonogram
(
    id           integer NOT NULL PRIMARY KEY IDENTITY,
    data_zajec   date    NOT NULL,
    start_zajec  time    NOT NULL,
    koniec_zajec time    NOT NULL,
    grupa_id     integer NOT NULL,
    przedmiot_id integer NOT NULL,
    FOREIGN KEY (grupa_id) REFERENCES grupa (id),
    FOREIGN KEY (przedmiot_id) REFERENCES przedmiot (id)
);

CREATE TABLE IF NOT EXISTS protokol
(
    id              integer              NOT NULL PRIMARY KEY IDENTITY,
    nr_protokolu    integer              NOT NULL,
    ocena           decimal              NOT NULL,
    status          boolean DEFAULT true NOT NULL,
    data_zamkniecia date,
    data_oceny      date,
    przedmiot_id    integer              NOT NULL,
    wykladowca_id   integer              NOT NULL,
    FOREIGN KEY (przedmiot_id) REFERENCES przedmiot (id),
    FOREIGN KEY (wykladowca_id) REFERENCES wykladowca (id)
);

CREATE TABLE IF NOT EXISTS wpis_do_indeksu
(
    id                               integer NOT NULL PRIMARY KEY IDENTITY,
    data_wpis                        date,
    ocena                            decimal,
    protokol_id                      integer NOT NULL,
    indeks_elektroniczny_studenta_id integer NOT NULL,
    FOREIGN KEY (protokol_id) REFERENCES protokol (id),
    FOREIGN KEY (indeks_elektroniczny_studenta_id) REFERENCES indeks_elektroniczny_studenta (id)
);

CREATE TABLE IF NOT EXISTS student_przedmiot
(
    id           integer NOT NULL PRIMARY KEY IDENTITY,
    student_id   integer NOT NULL,
    przedmiot_id integer NOT NULL,
    FOREIGN KEY (student_id) REFERENCES student (id),
    FOREIGN KEY (przedmiot_id) REFERENCES przedmiot (id)
);
