package pl.wwsis.utils;

import java.text.DecimalFormat;

public class NumberUtil {
    public static double parseToDouble(DecimalFormat format) throws NumberFormatException {
        String pattern = format.toPattern();
        return Double.parseDouble(pattern);
    }
}
