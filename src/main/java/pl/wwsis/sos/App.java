package pl.wwsis.sos;

import org.springframework.context.support.FileSystemXmlApplicationContext;
import pl.wwsis.sos.dao.*;
import org.springframework.context.ApplicationContext;
import pl.wwsis.sos.model.*;
import pl.wwsis.sos.model.enums.KierunekStudiow;

import java.time.LocalDate;
import java.util.List;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml");

//        adres(context);
//        osoba(context);
//        grupa(context);
//        konto(context);
//        student(context);
//        pracownik(context);
//        ksiazka(context);
//        autor(context);
//        wykladowca(context);
//        egzemplarz(context);
//        legitymacjaStudenta(context);
//        przedmiot(context);
//        wypozyczenie(context);
//        student(context);
//        indexElektronicznyStudenta(context);
//        harmonogram(context);
//        protokol(context);
        studentPrzedmiot(context);
    }

    public static void adres(ApplicationContext context) {
        AdresDao adresDao = (AdresDao) context.getBean("adresDao");
//        Adres adresList = adresDao.findById(1);

        String adresDetails = adresDao.showDetails(1);
        System.out.println(adresDetails);

//        for (Adres adres : adresList) {
//            System.out.println(adres.getKraj());
//        }
    }

    public static void osoba(ApplicationContext context) {
        OsobaDao osobaDao = (OsobaDao) context.getBean("osobaDao");

        System.out.println(osobaDao.pobierzImieINazwisko(1));
        System.out.println(osobaDao.pobierzInfoAdres(1));

        Osoba osoba = osobaDao.findById(1);
        Adres adres = osoba.getAdres();
        adres.setMiasto("Asdf");
        osobaDao.ustawNowyAdres(osoba, adres);

        System.out.println(osobaDao.pobierzInfoAdres(1));
    }

    public static void grupa(ApplicationContext context) {
        GrupaDao grupaDao = (GrupaDao) context.getBean("grupaDao");

        for (Object[] student : grupaDao.wyswietlStudentow(KierunekStudiow.INFORMATYKA)) {
            String imie = student[0].toString();
            String nazwisko = student[1].toString();
            String nrIndeksu = student[2].toString();

            System.out.printf("Imię: %s, Nazwisko: %s, Nr Indeksu: %s%n", imie, nazwisko, nrIndeksu);
        }
    }

    public static void konto(ApplicationContext context) {
        KontoDao kontoDao = (KontoDao) context.getBean("kontoDao");

        //DEACTIVATE
        kontoDao.wylaczKonto(2);

        // UPDATE
        Konto konto = kontoDao.findById(1);

        if (konto != null) {
            konto.setLogin("nowyLogin");
            konto.setHaslo("noweHaslo");

            kontoDao.update(konto);

            System.out.println("OK.");
        } else {
            System.out.println("Konto nie istnieje");
        }
    }

    public static void student(ApplicationContext context) {
        StudentDao studentDao = (StudentDao) context.getBean("studentDao");

        List<Student> students = studentDao.wyszukajStudenta("karol");

        for (Student student : students) {
            String imie = student.getOsoba().getImie();
            String nazwisko = student.getOsoba().getNazwisko();

            System.out.printf("Imię: %s, Nazwisko: %s", imie, nazwisko);
            System.out.println();
        }

        studentDao.archiveStudent(studentDao.findById(1));
    }

    public static void pracownik(ApplicationContext context) {
        PracownikDao pracownikDao = (PracownikDao) context.getBean("pracownikDao");

        List<Pracownik> pracownikList = pracownikDao.wyszukajPracownika("monika", "lewandowska");

        for (Pracownik pracownik : pracownikList) {
            String imie = pracownik.getOsoba().getImie();
            String nazwisko = pracownik.getOsoba().getNazwisko();

            System.out.printf("Imię: %s, Nazwisko: %s", imie, nazwisko);
            System.out.println();
        }
    }

    //Szczegóły Ksiazka
    public static void ksiazka(ApplicationContext context) {
        System.out.println("Test - szczegóły Książki:");

        KsiazkaDao ksiazkaDao = (KsiazkaDao) context.getBean("ksiazkaDao");

        Ksiazka ksiazka = ksiazkaDao.findById(5);
        System.out.println("id: " + ksiazka.getId() +
                ", tytuł: " + ksiazka.getTytul() +
                ", kategoria: " + ksiazka.getKategoria() +
                ", wydanie: " + ksiazka.getWydanie() +
                ", wydawnictwo: " + ksiazka.getWydawnictwo() +
                ", autor: " + ksiazka.getAutor().getImie() + " " + ksiazka.getAutor().getNazwisko() +
                ", data wydania: " + ksiazka.getDataWydania() +
                ", ilość stron: " + ksiazka.getIloscStron());

        for (Ksiazka ksiazka1 : ksiazkaDao.findBookByTitle("Podstawy fizyki")) {
            System.out.println(ksiazka1.getId());
        }
    }

    //Szczegóły Autor
    public static void autor(ApplicationContext context) {
        System.out.println("Test - szczegóły Autora:");

        AutorDao autorDao = (AutorDao) context.getBean("autorDao");

        Autor autor = autorDao.findById(4);
        System.out.println("id: " + autor.getId() +
                ", imie: " + autor.getImie() +
                ", nazwisko: " + autor.getNazwisko() +
                ", biografia: " + autor.getBiografia());
    }

    //Wyswietlenie wszystkich Wykladowcow
    public static void wykladowca(ApplicationContext context) {
        WykladowcaDao wykladowcaDao = (WykladowcaDao) context.getBean("wykladowcaDao");

        List<Wykladowca> wykladowcy = wykladowcaDao.findAll();

        StringBuilder stringBuilder = new StringBuilder();
        System.out.println("Test - Lista wykładowców: ");
        for (Wykladowca wykladowca : wykladowcy) {
            stringBuilder.append(String.format(
                    "ID: %d, Imię i nazwisko: %s %s, Specjalizacja: %s, Doświadczenie: %s, Stopień Naukowy: %s, Data zatrudnienia: %s%n",
                    wykladowca.getId(),
                    wykladowca.getPracownik().getOsoba().getImie(),
                    wykladowca.getPracownik().getOsoba().getNazwisko(),
                    wykladowca.getSpecjalizacja(),
                    wykladowca.getDoswiadczenie(),
                    wykladowca.getStopienNaukowy(),
                    wykladowca.getPracownik().getDataZatrudnienia()
            ));
        }
        System.out.println(stringBuilder.toString());

    }

    //Wyswietlenie dostępnych Egzemplarzy
    public static void egzemplarz(ApplicationContext context) {
        System.out.println("Test - Wyświetl dostępne Egzemplarze:");

        EgzemplarzDao egzemplarzDao = (EgzemplarzDao) context.getBean("egzemplarzDao");

        List<Egzemplarz> egzemplarzeDostepneInfo = egzemplarzDao.showAvailableEgzemplarze();
        StringBuilder stringBuilder = new StringBuilder();

        for (Egzemplarz egzemplarz : egzemplarzeDostepneInfo) {
            stringBuilder.append(String.format("ID: %d, Id egzemplarza: %s, Tytuł: %s, Data Wydania %s, Stan: %s%n",
                    egzemplarz.getId(),
                    egzemplarz.getIdEgzemplarza(),
                    egzemplarz.getKsiazka().getTytul(),
                    egzemplarz.getKsiazka().getDataWydania(),
                    egzemplarz.getStan()
            ));
        }
        System.out.println(stringBuilder.toString());
    }

    //Sprawdzenie ważności legitamcji oraz będzie przedłużenie legitmacji studenta
    public static void legitymacjaStudenta(ApplicationContext context) {
        //Sprawdzenie ważności legitamcji za pomocą numeru legitymacji:
        int numerLegitymacji1 = 1001;
        System.out.println("Test - Sprawdzenie ważności legitymacji o numerze: " + numerLegitymacji1);

        LegitymacjaStudentaDao legitymacjaStudentaDao = (LegitymacjaStudentaDao) context.getBean("legitymacjaStudentaDao");

        boolean isWazna = legitymacjaStudentaDao.sprawdzWaznosc(numerLegitymacji1);

        if (isWazna) {
            System.out.println("Legitymacja o numerze " + numerLegitymacji1 + " jest ważna.");
        } else {
            System.out.println("Legitymacja o numerze " + numerLegitymacji1 + " nie jest ważna.");
        }

        //Przedłużenie legitmacji studenta za pomocą numeru legitymacji
        int numerLegitymacji2 = 1001;
        int przedluzenieMiesiace = 20;
        System.out.println("Test - Przedłużenie ważności legitymacji o numerze: " + numerLegitymacji2);
        legitymacjaStudentaDao.przedluzWaznosc(numerLegitymacji2, przedluzenieMiesiace);
    }

    //Sprawdzenie metod klasy przedmiot
    public static void przedmiot(ApplicationContext context) {
        PrzedmiotDao przedmiotDao = (PrzedmiotDao) context.getBean("przedmiotDao");
        List<Przedmiot> przedmioty = przedmiotDao.wyswietlPrzedmioty(KierunekStudiow.INFORMATYKA, 1);

        System.out.println("Test modelu przedmiot > getNazwaPrzedmiotu:");
        System.out.println(przedmioty.get(0).getNazwaPrzedmiotu());

        System.out.println("Test modelu przedmiot > przedmiotyPoNazwie:");
        List<Przedmiot> przedmiotyPoNazwie = przedmiotDao.wyszukajPrzedmiotyPoNazwie("Fizyka Kwantowa");
        System.out.println(przedmiotyPoNazwie.get(0).getOpis());
    }

    public static void wypozyczenie(ApplicationContext context) {
        WypozyczenieDao wypozyczenieDao = (WypozyczenieDao) context.getBean("wypozyczenieDao");

        Wypozyczenie jakiesWypozyczenie = wypozyczenieDao.findById(2);

        System.out.println("Test modelu wypozyczenie > zarezerwuj:");
        LocalDate dataRezerwacji = LocalDate.of(2021, 1, 8);
        Wypozyczenie wypozyczenieZarezerwowane = wypozyczenieDao.zarezerwuj(jakiesWypozyczenie, dataRezerwacji);
        System.out.println(wypozyczenieZarezerwowane.getDataRezerwacji());

        System.out.println("Test modelu wypozyczenie > wypozycz:");
        LocalDate dataWypozyczenia = LocalDate.of(2022, 1, 8);
        Wypozyczenie wypozyczenieWypozyczone = wypozyczenieDao.wypozycz(jakiesWypozyczenie, dataWypozyczenia);
        System.out.println(wypozyczenieWypozyczone.getDataWypozyczenia());

        System.out.println("Test modelu wypozyczenie > zwroc:");
        LocalDate dataZwrotu = LocalDate.of(2020, 1, 8);
        Wypozyczenie wypozyczenieZwrocone = wypozyczenieDao.zwroc(jakiesWypozyczenie, dataZwrotu);
        System.out.println(wypozyczenieZwrocone.getDataOddania());

        System.out.println("Test modelu wypozyczenie > wydluzTerminOddania:");
        LocalDate nowyTermin = LocalDate.of(2025, 1, 8);
        Wypozyczenie wypozyczenieWydluzone = wypozyczenieDao.wydluzTerminOddania(jakiesWypozyczenie, nowyTermin);
        System.out.println(wypozyczenieWydluzone.getTerminOddania());
    }

    public static void indexElektronicznyStudenta(ApplicationContext context) {
        IndexElektronicznyStudentaDao indexElektronicznyStudentaDao = (IndexElektronicznyStudentaDao) context.getBean("indexElektronicznyStudentaDao");

        System.out.println("Test - Oblicz średnią ocen studenta Studenta:");
        System.out.println(indexElektronicznyStudentaDao.obliczSredniaOcen(1));

        System.out.println("Test - Wyświetl Listę przedmiotów z Wpisami Studenta:");
        List<Przedmiot> przedmiotyWpisyStudneta = indexElektronicznyStudentaDao.wyswietlListePrzedmiotowZWpisamiStudenta(1);

        for (Przedmiot przedmiot : przedmiotyWpisyStudneta) {
            System.out.println(przedmiot.getNazwaPrzedmiotu());
        }

        System.out.println("Test - Wyświetl Listę przedmiotów na które zapisany jest Student:");
        List<Przedmiot> przedmiotyStudenta = indexElektronicznyStudentaDao.wyswietlListePrzedmiotowNaKtoreZapisanyJestStudent("90123456789");

        for (Przedmiot przedmiot : przedmiotyStudenta) {
            System.out.println(przedmiot.getNazwaPrzedmiotu());
        }

    }

    public static void harmonogram(ApplicationContext context) {
        HarmonogramDao harmonogramDao = (HarmonogramDao) context.getBean("harmonogramDao");

        Integer nrIndexu = 1;
        String day = "2024-09-10";

        List<Object[]> harmonogram = harmonogramDao.getHarmonogram(nrIndexu, day);

        if (harmonogram.isEmpty()) {
            System.out.println("Brak harmongoramu");
        }

        for (Object[] object : harmonogram) {
            if (nrIndexu != null) {
                printHarmonogramStudent(object);
            } else {
                printHarmonogramGroup(object);
            }
            System.out.println();
        }
    }

    private static void printHarmonogramGroup(Object[] object) {
        System.out.println("nazwa kierunku: " + object[0]);
        System.out.println("semestr: " + object[1]);
        System.out.println("przedmiot: " + object[2]);
        System.out.println("data: " + object[3]);
        System.out.println("start: " + object[4]);
        System.out.println("koniec: " + object[5]);
    }

    private static void printHarmonogramStudent(Object[] object) {
        System.out.println("przedmiot: " + object[0]);
        System.out.println("data: " + object[1]);
        System.out.println("start: " + object[2]);
        System.out.println("koniec: " + object[3]);
    }

    public static void protokol(ApplicationContext context) {
        ProtokolDao protokolDao = (ProtokolDao) context.getBean("protokolDao");
        int nrProtokolu = 101;
        System.out.println("Test - Zamknij protokol o numerze " + nrProtokolu + " ,statusie: " +
                protokolDao.getProtokolWedlugNrProtokolu(nrProtokolu).getStatus());
        protokolDao.zamknijProtokol(nrProtokolu);
        System.out.println("Po: " + protokolDao.getProtokolWedlugNrProtokolu(nrProtokolu).getStatus());

        System.out.println("Test - Otwórz protokol o numerze " + nrProtokolu + " ,statusie: " +
                protokolDao.getProtokolWedlugNrProtokolu(nrProtokolu).getStatus());
        protokolDao.otworzProtokol(nrProtokolu);
        System.out.println("Po: " + protokolDao.getProtokolWedlugNrProtokolu(nrProtokolu).getStatus());
    }

    public static void studentPrzedmiot(ApplicationContext context) {
        StudentDao studentDao = (StudentDao) context.getBean("studentDao");
        PrzedmiotDao przedmiotDao = (PrzedmiotDao) context.getBean("przedmiotDao");
        StudentPrzedmiotDao studentPrzedmiotDao = (StudentPrzedmiotDao) context.getBean("studentPrzedmiotDao");

        Student student = studentDao.findById(12);
        Przedmiot przedmiot = przedmiotDao.findById(2);

        zapisankoStudenta(context, student, przedmiot, studentPrzedmiotDao);

        try {
        System.out.println("Student jest zapisany na przedmiot: " + studentPrzedmiotDao.czyStudentJestWPrzedmiocie(student, przedmiot));
        studentPrzedmiotDao.wypiszStudenta(student, przedmiot);
        } catch (Exception e) {
                System.out.println(e.getMessage());
        }
    }

    public static void zapisankoStudenta(
            ApplicationContext context,
            Student student,
            Przedmiot przedmiot,
            StudentPrzedmiotDao studentPrzedmiotDao
    ) {
        StudentPrzedmiot studentPrzedmiot = new StudentPrzedmiot();
        studentPrzedmiot.setStudent(student);
        studentPrzedmiot.setPrzedmiot(przedmiot);

        studentPrzedmiotDao.save(studentPrzedmiot);
    }
}
