package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Konto;

import java.util.List;

public interface KontoDao {
    void save(Konto konto);
    Konto findById(Integer id);
    List<Konto> findAll();
    void update(Konto konto);
    void delete(Konto konto);
    void wylaczKonto(Integer kontoId);
}
