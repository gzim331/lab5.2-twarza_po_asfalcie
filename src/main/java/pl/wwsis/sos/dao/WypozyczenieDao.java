package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Wypozyczenie;

import java.time.LocalDate;
import java.util.List;

public interface WypozyczenieDao {
    void save(Wypozyczenie wypozyczenie);
    Wypozyczenie findById(Integer id);
    List<Wypozyczenie> findAll();
    void update(Wypozyczenie wypozyczenie);
    void delete(Wypozyczenie wypozyczenie);
    Wypozyczenie zarezerwuj(Wypozyczenie wypozyczenie, LocalDate dataRezerwacji);
    Wypozyczenie wypozycz(Wypozyczenie wypozyczenie, LocalDate dataWypozyczenia);
    Wypozyczenie zwroc(Wypozyczenie wypozyczenie, LocalDate dataOddania);
    Wypozyczenie wydluzTerminOddania(Wypozyczenie wypozyczenie, LocalDate terminOddania);
}
