package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Egzemplarz;
import pl.wwsis.sos.model.Ksiazka;

import java.util.List;

public interface EgzemplarzDao {
    void save(Egzemplarz egzemplarz);
    Egzemplarz findById(Integer id);
    List<Egzemplarz> findAll();
    void update(Egzemplarz egzemplarz);
    void delete(Egzemplarz egzemplarz);
    List <Egzemplarz> showAvailableEgzemplarze();
    void dodajKsiazke(Ksiazka ksiazka, String idEgzemplarza);
}
