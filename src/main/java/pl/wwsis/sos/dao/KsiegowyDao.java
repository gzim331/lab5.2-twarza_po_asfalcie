package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Ksiegowy;

import java.util.List;

public interface KsiegowyDao {
    void save(Ksiegowy ksiegowy);
    Ksiegowy findById(Integer id);
    List<Ksiegowy> findAll();
    void update(Ksiegowy ksiegowy);
    void delete(Ksiegowy ksiegowy);
}
