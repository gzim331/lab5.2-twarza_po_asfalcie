package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Harmonogram;

import java.util.List;

public interface HarmonogramDao {
    void save(Harmonogram harmonogram);
    Harmonogram findById(Integer id);
    List<Harmonogram> findAll();
    void update(Harmonogram harmonogram);
    void delete(Harmonogram harmonogram);
    List<Object[]> getHarmonogram(Integer nrIndexu, String day);
}
