package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.WpisDoIndexu;

import java.util.List;

public interface WpisDoIndexuDao {
    void save(WpisDoIndexu wpisDoIndexu);
    WpisDoIndexu findById(Integer id);
    List<WpisDoIndexu> findAll();
    void update(WpisDoIndexu wpisDoIndexu);
    void delete(WpisDoIndexu wpisDoIndexu);
}
