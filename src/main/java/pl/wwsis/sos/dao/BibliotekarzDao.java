package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Bibliotekarz;

import java.util.List;

public interface BibliotekarzDao {
    void save(Bibliotekarz bibliotekarz);
    Bibliotekarz findById(Integer id);
    List<Bibliotekarz> findAll();
    void update(Bibliotekarz bibliotekarz);
    void delete(Bibliotekarz bibliotekarz);
}
