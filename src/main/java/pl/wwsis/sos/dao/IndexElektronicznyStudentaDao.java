package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.IndexElektronicznyStudenta;
import pl.wwsis.sos.model.Przedmiot;

import java.util.List;

public interface IndexElektronicznyStudentaDao {
    void save(IndexElektronicznyStudenta indexElektronicznyStudenta);
    IndexElektronicznyStudenta findById(Integer id);
    List<IndexElektronicznyStudenta> findAll();
    void update(IndexElektronicznyStudenta indexElektronicznyStudenta);
    void delete(IndexElektronicznyStudenta indexElektronicznyStudenta);
    double obliczSredniaOcen(Integer nrIndexu);
    List<Przedmiot> wyswietlListePrzedmiotowZWpisamiStudenta(Integer nrIndexuStudenta);
    List<Przedmiot> wyswietlListePrzedmiotowNaKtoreZapisanyJestStudent(String pesel);
}
