package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Grupa;
import pl.wwsis.sos.model.enums.KierunekStudiow;

import java.util.List;

public interface GrupaDao {
    void save(Grupa grupa);
    Grupa findById(Integer id);
    List<Grupa> findAll();
    void update(Grupa grupa);
    void delete(Grupa grupa);
    List<Object[]> wyswietlStudentow(KierunekStudiow nazwaKierunku);
}
