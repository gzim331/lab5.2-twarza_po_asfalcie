package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.OperacjaFinansowa;

import java.util.List;

public interface OperacjaFinansowaDao {
    void save(OperacjaFinansowa operacjaFinansowa);
    OperacjaFinansowa findById(Integer id);
    List<OperacjaFinansowa> findAll();
    void update(OperacjaFinansowa operacjaFinansowa);
    void delete(OperacjaFinansowa operacjaFinansowa);
}
