package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Pracownik;

import java.util.List;

public interface PracownikDao {
    void save(Pracownik pracownik);
    Pracownik findById(Integer id);
    List<Pracownik> findAll();
    void update(Pracownik pracownik);
    void delete(Pracownik pracownik);
    List<Pracownik> wyszukajPracownika(String imie, String nazwisko);
}
