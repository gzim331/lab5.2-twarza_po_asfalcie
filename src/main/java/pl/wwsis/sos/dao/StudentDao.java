package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Student;

import java.util.List;

public interface StudentDao {
    void save(Student student);
    Student findById(Integer id);
    List<Student> findAll();
    void update(Student student);
    void delete(Student student);
    List<Student> wyszukajStudenta(String searchText);
    void archiveStudent(Student student);
}
