package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.PracownikDao;
import pl.wwsis.sos.model.Pracownik;
import pl.wwsis.sos.model.Student;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class PracownikDaoImpl implements PracownikDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Pracownik pracownik) {
        entityManager.persist(pracownik);
    }

    @Override
    public Pracownik findById(Integer id) {
        return entityManager.find(Pracownik.class, id);
    }

    @Override
    public List<Pracownik> findAll() {
        return entityManager.createQuery("SELECT a FROM Pracownik a", Pracownik.class).getResultList();
    }

    @Override
    public void update(Pracownik pracownik) {
        entityManager.merge(pracownik);
    }

    @Override
    public void delete(Pracownik pracownik) {
        entityManager.remove(entityManager.contains(pracownik) ? pracownik : entityManager.merge(pracownik));
    }

    @Override
    public List<Pracownik> wyszukajPracownika(String imie, String nazwisko) {
        String queryString = "SELECT p FROM Pracownik p " +
                "JOIN p.osoba o " +
                "WHERE LOWER(o.imie) = LOWER(:imie) AND " +
                "LOWER(o.nazwisko) = LOWER(:nazwisko)";

        Query query = entityManager.createQuery(queryString, Pracownik.class);
        query.setParameter("imie", imie.toLowerCase());
        query.setParameter("nazwisko", nazwisko.toLowerCase());

        return query.getResultList();
    }
}