package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.PrzedmiotDao;
import pl.wwsis.sos.model.Przedmiot;
import pl.wwsis.sos.model.enums.KierunekStudiow;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class PrzedmiotDaoImpl implements PrzedmiotDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Przedmiot przedmiot) {
        entityManager.persist(przedmiot);
    }

    @Override
    public Przedmiot findById(Integer id) {
        return entityManager.find(Przedmiot.class, id);
    }

    @Override
    public List<Przedmiot> findAll() {
        String queryString = "SELECT a FROM Przedmiot a";
        Query query = entityManager.createQuery(queryString);
        return query.getResultList();
    }

    @Override
    public void update(Przedmiot przedmiot) {
        entityManager.merge(przedmiot);
    }

    @Override
    public void delete(Przedmiot przedmiot) {
        entityManager.remove(entityManager.contains(przedmiot) ? przedmiot : entityManager.merge(przedmiot));
    }

    @Override
    public List<Przedmiot> wyswietlPrzedmioty(KierunekStudiow kierunekStudiow, Integer semestr) {
        String queryString = "SELECT a FROM Przedmiot a " +
                " WHERE a.kierunekStudiow = :kierunekStudiow" +
                " AND a.semestr = :semestr";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("kierunekStudiow", kierunekStudiow);
        query.setParameter("semestr", semestr);
        return query.getResultList();
    }

    @Override
    public List<Przedmiot> wyszukajPrzedmiotyPoNazwie(String nazwaPrzedmiotu) {
        String queryString = "SELECT a FROM Przedmiot a WHERE a.nazwaPrzedmiotu = :nazwaPrzedmiotu";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("nazwaPrzedmiotu", nazwaPrzedmiotu);
        return query.getResultList();
    }
}