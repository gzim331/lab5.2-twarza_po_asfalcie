package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.ProtokolDao;
import pl.wwsis.sos.model.Protokol;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class ProtokolDaoImpl implements ProtokolDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Protokol protokol) {
        entityManager.persist(protokol);
    }

    @Override
    public Protokol findById(Integer id) {
        return entityManager.find(Protokol.class, id);
    }

    @Override
    public List<Protokol> findAll() {
        return entityManager.createQuery("SELECT a FROM Protokol a", Protokol.class).getResultList();
    }

    @Override
    public void update(Protokol protokol) {
        entityManager.merge(protokol);
    }

    @Override
    public void delete(Protokol protokol) {
        entityManager.remove(entityManager.contains(protokol) ? protokol : entityManager.merge(protokol));
    }

    @Override
    public Protokol getProtokolWedlugNrProtokolu(Integer nrProtokolu) {
        Protokol protokol = entityManager.createQuery(
                        "SELECT p FROM Protokol p WHERE p.nrProtokolu = :nrProtokolu", Protokol.class)
                .setParameter("nrProtokolu", nrProtokolu)
                .getSingleResult();
        return protokol;
    }

    @Override
    public void otworzProtokol(Integer nrProtokolu) {
        Protokol protokol = getProtokolWedlugNrProtokolu(nrProtokolu);

        protokol.setStatus(true);
        entityManager.merge(protokol);
    }


    @Override
    public void zamknijProtokol(Integer nrProtokolu) {
        Protokol protokol =getProtokolWedlugNrProtokolu(nrProtokolu);

        protokol.setStatus(false);
        entityManager.merge(protokol);
    }
}