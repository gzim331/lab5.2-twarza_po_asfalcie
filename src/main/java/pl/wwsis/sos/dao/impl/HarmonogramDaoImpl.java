package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.HarmonogramDao;
import pl.wwsis.sos.model.Harmonogram;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Transactional
public class HarmonogramDaoImpl implements HarmonogramDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Harmonogram harmonogram) {
        entityManager.persist(harmonogram);
    }

    @Override
    public Harmonogram findById(Integer id) {
        return entityManager.find(Harmonogram.class, id);
    }

    @Override
    public List<Harmonogram> findAll() {
        return entityManager.createQuery("SELECT a FROM Harmonogram a", Harmonogram.class).getResultList();
    }

    @Override
    public void update(Harmonogram harmonogram) {
        entityManager.merge(harmonogram);
    }

    @Override
    public void delete(Harmonogram harmonogram) {
        entityManager.remove(entityManager.contains(harmonogram) ? harmonogram : entityManager.merge(harmonogram));
    }

    @Override
    public List<Object[]> getHarmonogram(Integer nrIndexu, String day) {
        StringBuilder sql = new StringBuilder();
        if (nrIndexu != null) {
            sql.append("select p.NAZWA_PRZEDMIOTU, h.DATA_ZAJEC, h.START_ZAJEC, h.KONIEC_ZAJEC " +
                    "from PRZEDMIOT p " +
                    "inner join HARMONOGRAM h on p.ID = h.PRZEDMIOT_ID " +
                    "inner join GRUPA g on h.GRUPA_ID = g.ID " +
                    "inner join STUDENT s on g.ID = s.GRUPA_ID " +
                    "inner join INDEKS_ELEKTRONICZNY_STUDENTA ies on s.ID = ies.STUDENT_ID " +
                    "where ies.NR_INDEKSU = :nrIndeksu ");
            if (day != null) {
                sql.append("and h.DATA_ZAJEC = :day ");
            }
        } else {
            sql.append("select g.NAZWA_KIERUNKU, g.SEMESTR, p.NAZWA_PRZEDMIOTU, h.DATA_ZAJEC, h.START_ZAJEC, h.KONIEC_ZAJEC " +
                    "from PRZEDMIOT p " +
                    "inner join HARMONOGRAM h on p.ID = h.PRZEDMIOT_ID " +
                    "inner join GRUPA g on h.GRUPA_ID = g.ID ");
            if (day != null) {
                sql.append("where h.DATA_ZAJEC = :day ");
            }
        }

        Query query = entityManager.createNativeQuery(sql.toString());

        if (nrIndexu != null) {
            query.setParameter("nrIndeksu", nrIndexu);
        }
        if (day != null) {
            query.setParameter("day", day);
        }

        return query.getResultList();
    }
}