package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.AdresDao;
import pl.wwsis.sos.model.Adres;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class AdresDaoImpl implements AdresDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Adres adres) {
        entityManager.persist(adres);
    }

    @Override
    public Adres findById(Integer id) {
        return entityManager.find(Adres.class, id);
    }

    @Override
    public List<Adres> findAll() {
        return entityManager.createQuery("SELECT a FROM Adres a", Adres.class).getResultList();
    }

    @Override
    public void update(Adres adres) {
        entityManager.merge(adres);
    }

    @Override
    public void delete(Adres adres) {
        entityManager.remove(entityManager.contains(adres) ? adres : entityManager.merge(adres));
    }

    @Override
    public String showDetails(Integer id) {
        Adres adres = entityManager.find(Adres.class, id);
        return "id: " + adres.getId() +
                ", kraj: " + adres.getKraj() +
                ", miasto: " + adres.getMiasto() +
                ", ulica: " + adres.getUlica() +
                ", nr domu: " + adres.getNrDomu() +
                ", nr mieszkania: " + adres.getNrMieszkania();
    }
}