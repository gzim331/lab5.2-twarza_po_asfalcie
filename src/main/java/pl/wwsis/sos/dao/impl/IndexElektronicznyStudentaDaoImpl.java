package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.IndexElektronicznyStudentaDao;
import pl.wwsis.sos.model.IndexElektronicznyStudenta;
import pl.wwsis.sos.model.Przedmiot;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class IndexElektronicznyStudentaDaoImpl implements IndexElektronicznyStudentaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(IndexElektronicznyStudenta indexElektronicznyStudenta) {
        entityManager.persist(indexElektronicznyStudenta);
    }

    @Override
    public IndexElektronicznyStudenta findById(Integer id) {
        return entityManager.find(IndexElektronicznyStudenta.class, id);
    }

    @Override
    public List<IndexElektronicznyStudenta> findAll() {
        return entityManager.createQuery("SELECT a FROM IndexElektronicznyStudenta a", IndexElektronicznyStudenta.class).getResultList();
    }

    @Override
    public void update(IndexElektronicznyStudenta indexElektronicznyStudenta) {
        entityManager.merge(indexElektronicznyStudenta);
    }

    @Override
    public void delete(IndexElektronicznyStudenta indexElektronicznyStudenta) {
        entityManager.remove(entityManager.contains(indexElektronicznyStudenta) ? indexElektronicznyStudenta : entityManager.merge(indexElektronicznyStudenta));
    }

    @Override
    public double obliczSredniaOcen(Integer nrIndexu) {
        // Pobranie średniej ocen studenta o danym nrIndexu
        String sql = "SELECT AVG(w.ocena) " +
                     "FROM WpisDoIndexu w " +
                     "JOIN w.indexElektronicznyStudenta ies " +
                     "WHERE ies.nrIndeksu = :nrIndexu";

        // Wykonanie zapytania i pobranie wyniku
        Object result = entityManager.createQuery(sql)
                .setParameter("nrIndexu", nrIndexu)
                .getSingleResult();

        // Konwersja wyniku na double
        return result != null ? ((Number) result).doubleValue() : 0.0;
    }


    @Override
    public List<Przedmiot> wyswietlListePrzedmiotowZWpisamiStudenta(Integer nrIndexu) {
        String sql = "SELECT p " +
                     "FROM WpisDoIndexu w " +
                     "JOIN w.protokol pr " +
                     "JOIN pr.przedmiot p " +
                     "JOIN w.indexElektronicznyStudenta ies " +
                     "WHERE ies.nrIndeksu = :nrIndexu";

        return entityManager.createQuery(sql, Przedmiot.class)
                .setParameter("nrIndexu", nrIndexu)
                .getResultList();
    }

    @Override
    public List<Przedmiot> wyswietlListePrzedmiotowNaKtoreZapisanyJestStudent(String pesel) {
        String sql = "SELECT p " +
                     "FROM IndexElektronicznyStudenta s " +
                     "JOIN s.student o " +
                     "JOIN o.grupa g " +
                     "INNER JOIN Harmonogram h ON g.id = h.id " +
                     "INNER JOIN Przedmiot p ON h.id = p.id " +
                     "WHERE s.id = (SELECT id FROM Student WHERE osoba.id = (SELECT id FROM Osoba WHERE pesel = :pesel))";

        return entityManager.createQuery(sql, Przedmiot.class)
                .setParameter("pesel", pesel)
                .getResultList();
    }

}