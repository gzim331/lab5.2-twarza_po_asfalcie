package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.OsobaDao;
import pl.wwsis.sos.model.Adres;
import pl.wwsis.sos.model.Osoba;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class OsobaDaoImpl implements OsobaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Osoba osoba) {
        entityManager.persist(osoba);
    }

    @Override
    public Osoba findById(Integer id) {
        return entityManager.find(Osoba.class, id);
    }

    @Override
    public List<Osoba> findAll() {
        return entityManager.createQuery("SELECT a FROM Osoba a", Osoba.class).getResultList();
    }

    @Override
    public void update(Osoba osoba) {
        entityManager.merge(osoba);
    }

    @Override
    public void delete(Osoba osoba) {
        entityManager.remove(entityManager.contains(osoba) ? osoba : entityManager.merge(osoba));
    }

    @Override
    public String pobierzImieINazwisko(Integer osobaId) {
        Osoba osoba = entityManager.find(Osoba.class, osobaId);

        return "imie: " + osoba.getImie() + ", nazwisko: " + osoba.getNazwisko();
    }

    @Override
    public String pobierzInfoAdres(Integer osobaId) {
        String queryString = "SELECT a.kraj, a.miasto, a.ulica, a.nrDomu, a.nrMieszkania FROM Osoba o " +
                "INNER JOIN o.adres a " +
                "WHERE o.id = :osobaId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("osobaId", osobaId);

        List<Object[]> results = query.getResultList();

        if (results.isEmpty()) {
            System.out.println("Brak wyników dla podanego ID: " + osobaId);
            return "Brak wyników dla podanego ID";
        }

        Object[] adres = results.get(0);

        return "kraj: " + adres[0] + ", miasto: " + adres[1] + ", ulica: " + adres[2] +
                ", nr domu: " + adres[3] + ", nr mieszkania: " + adres[4];
    }

    @Override
    public void ustawNowyAdres(Osoba osoba, Adres nowyAdres) {
        if (osoba.getAdres() != null) {
            Adres staryAdres = entityManager.merge(osoba.getAdres());
            entityManager.remove(staryAdres);
        }

        if (entityManager.contains(nowyAdres)) {
            osoba.setAdres(nowyAdres);
        } else {
            Adres adres = entityManager.merge(nowyAdres);
            osoba.setAdres(adres);
        }

        entityManager.merge(osoba);
        entityManager.flush();
    }
}