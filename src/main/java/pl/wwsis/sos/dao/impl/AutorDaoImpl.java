package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.AutorDao;
import pl.wwsis.sos.model.Autor;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class AutorDaoImpl implements AutorDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Autor autor) {
        entityManager.persist(autor);
    }

    @Override
    public Autor findById(Integer id) {
        return entityManager.find(Autor.class, id);
    }

    @Override
    public List<Autor> findAll() {
        return entityManager.createQuery("SELECT a FROM Autor a", Autor.class).getResultList();
    }

    @Override
    public void update(Autor autor) {
        entityManager.merge(autor);
    }

    @Override
    public void delete(Autor autor) {
        entityManager.remove(entityManager.contains(autor) ? autor : entityManager.merge(autor));
    }
/*
    @Override
    public String showDetails(Integer id) {
        Autor autor = entityManager.find(Autor.class, id);
        return "id: " + autor.getId() +
               ", imie: " + autor.getImie() +
               ", nazwisko: " + autor.getNazwisko() +
               ", biografia: " + autor.getBiografia();
    }
    */
}