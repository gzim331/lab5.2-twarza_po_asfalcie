package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.KontoDao;
import pl.wwsis.sos.model.Konto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class KontoDaoImpl implements KontoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Konto konto) {
        entityManager.persist(konto);
    }

    @Override
    public Konto findById(Integer id) {
        return entityManager.find(Konto.class, id);
    }

    @Override
    public List<Konto> findAll() {
        return entityManager.createQuery("SELECT a FROM Konto a", Konto.class).getResultList();
    }

    @Override
    public void update(Konto konto) {
        entityManager.merge(konto);
    }

    @Override
    public void delete(Konto konto) {
        entityManager.remove(entityManager.contains(konto) ? konto : entityManager.merge(konto));
    }

    @Override
    public void wylaczKonto(Integer kontoId) {
        Konto konto = findById(kontoId);

        if (konto != null) {
            konto.setStatus(false);
            update(konto);
        } else {
            System.out.println("Konto nie istnieje");
        }
    }
}