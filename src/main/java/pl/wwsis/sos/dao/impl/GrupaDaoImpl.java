package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.GrupaDao;
import pl.wwsis.sos.model.Grupa;
import pl.wwsis.sos.model.enums.KierunekStudiow;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class GrupaDaoImpl implements GrupaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Grupa grupa) {
        entityManager.persist(grupa);
    }

    @Override
    public Grupa findById(Integer id) {
        return entityManager.find(Grupa.class, id);
    }

    @Override
    public List<Grupa> findAll() {
        return entityManager.createQuery("SELECT a FROM Grupa a", Grupa.class).getResultList();
    }

    @Override
    public void update(Grupa grupa) {
        entityManager.merge(grupa);
    }

    @Override
    public void delete(Grupa grupa) {
        entityManager.remove(entityManager.contains(grupa) ? grupa : entityManager.merge(grupa));
    }

    @Override
    public List<Object[]> wyswietlStudentow(KierunekStudiow nazwaKierunku) {
        String queryString = "SELECT o.imie, o.nazwisko, ies.nrIndeksu FROM Grupa g " +
                "        INNER JOIN Student s ON g.id = s.id " +
                "        INNER JOIN Osoba o ON s.id = o.id " +
                "        INNER JOIN IndexElektronicznyStudenta ies ON s.id = ies.id " +
                "        WHERE g.nazwaKierunku = :nazwaKierunku";

        Query query = entityManager.createQuery(queryString);
        query.setParameter("nazwaKierunku", nazwaKierunku);

        return query.getResultList();
    }
}