package pl.wwsis.sos.dao.impl;


import pl.wwsis.sos.dao.WypozyczenieDao;
import pl.wwsis.sos.model.Wypozyczenie;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public class WypozyczenieDaoImpl implements WypozyczenieDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Wypozyczenie wypozyczenie) {
        entityManager.persist(wypozyczenie);
        entityManager.flush();
    }

    @Override
    public Wypozyczenie findById(Integer id) {
        return entityManager.find(Wypozyczenie.class, id);
    }

    @Override
    public List<Wypozyczenie> findAll() {
        return entityManager.createQuery("SELECT a FROM Wypozyczenie a", Wypozyczenie.class).getResultList();
    }

    @Override
    public void update(Wypozyczenie wypozyczenie) {
        entityManager.merge(wypozyczenie);
    }

    @Override
    public void delete(Wypozyczenie wypozyczenie) {
        entityManager.remove(entityManager.contains(wypozyczenie) ? wypozyczenie : entityManager.merge(wypozyczenie));
    }

    @Override
    public Wypozyczenie zarezerwuj(Wypozyczenie wypozyczenie, LocalDate dataRezerwacji)
    {
        wypozyczenie.setDataRezerwacji(dataRezerwacji);
        entityManager.merge(wypozyczenie);
        return wypozyczenie;
    }

    @Override
    public Wypozyczenie wypozycz(Wypozyczenie wypozyczenie, LocalDate dataWypozyczenia) {
        wypozyczenie.setDataWypozyczenia(dataWypozyczenia);
        entityManager.merge(wypozyczenie);
        return wypozyczenie;
    }

    @Override
    public Wypozyczenie zwroc(Wypozyczenie wypozyczenie, LocalDate dataOddania) {
        wypozyczenie.setDataOddania(dataOddania);
        entityManager.merge(wypozyczenie);
        return wypozyczenie;
    }

    @Override
    public Wypozyczenie wydluzTerminOddania(Wypozyczenie wypozyczenie, LocalDate terminOddania) {
        wypozyczenie.setTerminOddania(terminOddania);
        entityManager.merge(wypozyczenie);
        return wypozyczenie;
    }
}