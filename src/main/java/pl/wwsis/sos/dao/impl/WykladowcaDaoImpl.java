package pl.wwsis.sos.dao.impl;


import pl.wwsis.sos.dao.WykladowcaDao;
import pl.wwsis.sos.model.Wykladowca;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class WykladowcaDaoImpl implements WykladowcaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Wykladowca wykladowca) {
        entityManager.persist(wykladowca);
    }

    @Override
    public Wykladowca findById(Integer id) {
        return entityManager.find(Wykladowca.class, id);
    }

    @Override
    public List<Wykladowca> findAll() {
        return entityManager.createQuery("SELECT a FROM Wykladowca a", Wykladowca.class).getResultList();
    }

    @Override
    public void update(Wykladowca wykladowca) {
        entityManager.merge(wykladowca);
    }

    @Override
    public void delete(Wykladowca wykladowca) {
        entityManager.remove(entityManager.contains(wykladowca) ? wykladowca : entityManager.merge(wykladowca));
    }
/*
    //Niepotrzebna metoda:
    @Override
    public String showAllWykladowca() {
        List<Wykladowca> wykladowcy = findAll();

        StringBuilder stringBuilder = new StringBuilder();

        for (Wykladowca wykladowca : wykladowcy) {
            stringBuilder.append(String.format("ID: %d, Imię i nazwisko: %s %s, " +
                                               "Specjalizacja: %s, Doświadczenie: %s, Stopień Naukowy: %s, " +
                                               "Data zatrudnienia: %s%n",
                    wykladowca.getId(),
                    wykladowca.getPracownik().getOsoba().getImie(),
                    wykladowca.getPracownik().getOsoba().getNazwisko(),
                    wykladowca.getSpecjalizacja(),
                    wykladowca.getDoswiadczenie(),
                    wykladowca.getStopienNaukowy(),
                    wykladowca.getPracownik().getDataZatrudnienia()));
        }

        return stringBuilder.toString();
    }
    */
}