package pl.wwsis.sos.dao.impl;


import pl.wwsis.sos.dao.StudentDao;
import pl.wwsis.sos.model.Student;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class StudentDaoImpl implements StudentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Student student) {
        entityManager.persist(student);
    }

    @Override
    public Student findById(Integer id) {
        return entityManager.find(Student.class, id);
    }

    @Override
    public List<Student> findAll() {
        return entityManager.createQuery("SELECT a FROM Student a", Student.class).getResultList();
    }

    @Override
    public void update(Student student) {
        entityManager.merge(student);
    }

    @Override
    public void delete(Student student) {
        entityManager.remove(entityManager.contains(student) ? student : entityManager.merge(student));
    }

    @Override
    public List<Student> wyszukajStudenta(String searchText) {
        String queryString = "SELECT s FROM Student s " +
                "INNER JOIN Osoba o ON s.id = o.id " +
                "INNER JOIN IndexElektronicznyStudenta ies ON s.id = ies.id  " +
                "WHERE LOWER(o.imie) LIKE LOWER(:searchText) OR " +
                "LOWER(o.nazwisko) LIKE LOWER(:searchText) OR " +
                "CAST(ies.nrIndeksu AS string) LIKE LOWER(:searchText) OR " +
                "LOWER(o.pesel) LIKE LOWER(:searchText) OR " +
                "LOWER(o.email) LIKE LOWER(:searchText)";

        Query query = entityManager.createQuery(queryString, Student.class);
        query.setParameter("searchText", "%" + searchText.toLowerCase() + "%");

        return query.getResultList();
    }

    @Override
    public void archiveStudent(Student student) {
        entityManager.createQuery("UPDATE Student s SET s.status = 'nieaktywny' WHERE s.id = :id")
                .setParameter("id", student.getId())
                .executeUpdate();
    }
}