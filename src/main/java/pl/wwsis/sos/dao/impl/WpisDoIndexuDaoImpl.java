package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.WpisDoIndexuDao;
import pl.wwsis.sos.model.WpisDoIndexu;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class WpisDoIndexuDaoImpl implements WpisDoIndexuDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(WpisDoIndexu wpisDoIndexu) {
        entityManager.persist(wpisDoIndexu);
    }

    @Override
    public WpisDoIndexu findById(Integer id) {
        return entityManager.find(WpisDoIndexu.class, id);
    }

    @Override
    public List<WpisDoIndexu> findAll() {
        return entityManager.createQuery("SELECT a FROM WpisDoIndexu a", WpisDoIndexu.class).getResultList();
    }

    @Override
    public void update(WpisDoIndexu wpisDoIndexu) {
        entityManager.merge(wpisDoIndexu);
    }

    @Override
    public void delete(WpisDoIndexu wpisDoIndexu) {
        entityManager.remove(entityManager.contains(wpisDoIndexu) ? wpisDoIndexu : entityManager.merge(wpisDoIndexu));
    }
}