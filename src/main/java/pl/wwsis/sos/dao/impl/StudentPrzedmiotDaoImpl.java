package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.StudentPrzedmiotDao;
import pl.wwsis.sos.model.Przedmiot;
import pl.wwsis.sos.model.Student;
import pl.wwsis.sos.model.StudentPrzedmiot;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

public class StudentPrzedmiotDaoImpl implements StudentPrzedmiotDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(StudentPrzedmiot studentPrzedmiot) {
        entityManager.persist(studentPrzedmiot);
    }

    @Override
    public StudentPrzedmiot findById(Integer id) {
        return entityManager.find(StudentPrzedmiot.class, id);
    }

    @Override
    public List<StudentPrzedmiot> findAll() {
        return entityManager.createQuery("SELECT a FROM StudentPrzedmiot a", StudentPrzedmiot.class).getResultList();
    }

    @Override
    public void update(StudentPrzedmiot studentPrzedmiot) {
        entityManager.merge(studentPrzedmiot);
    }

    @Override
    public void delete(StudentPrzedmiot studentPrzedmiot) {
        entityManager.remove(entityManager.contains(studentPrzedmiot) ? studentPrzedmiot : entityManager.merge(studentPrzedmiot));
    }

    @Override
    @org.springframework.transaction.annotation.Transactional
    public void wypiszStudenta(Student student, Przedmiot przedmiot) {
        int deletedCount = 0;
        try {
            String query = "DELETE FROM StudentPrzedmiot sp WHERE sp.student = :student AND sp.przedmiot = :przedmiot";
            deletedCount = entityManager.createQuery(query)
                    .setParameter("student", student)
                    .setParameter("przedmiot", przedmiot)
                    .executeUpdate();
        } catch (Exception e) {
        }

        if (deletedCount == 0) {
            System.out.println("No deleted entries");
        }
    }

    @Override
    public Boolean czyStudentJestWPrzedmiocie(Student student, Przedmiot przedmiot) {
        String query = "SELECT sp FROM StudentPrzedmiot sp WHERE sp.student = :student AND sp.przedmiot = :przedmiot";
        StudentPrzedmiot studentPrzedmiot = entityManager.createQuery(query, StudentPrzedmiot.class)
                .setParameter("student", student)
                .setParameter("przedmiot", przedmiot)
                .getSingleResult();

        return null != studentPrzedmiot;
    }
}
