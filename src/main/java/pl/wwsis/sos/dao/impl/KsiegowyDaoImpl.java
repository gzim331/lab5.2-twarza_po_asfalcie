package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.KsiegowyDao;
import pl.wwsis.sos.model.Ksiegowy;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class KsiegowyDaoImpl implements KsiegowyDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Ksiegowy ksiegowy) {
        entityManager.persist(ksiegowy);
    }

    @Override
    public Ksiegowy findById(Integer id) {
        return entityManager.find(Ksiegowy.class, id);
    }

    @Override
    public List<Ksiegowy> findAll() {
        return entityManager.createQuery("SELECT a FROM Ksiegowy a", Ksiegowy.class).getResultList();
    }

    @Override
    public void update(Ksiegowy ksiegowy) {
        entityManager.merge(ksiegowy);
    }

    @Override
    public void delete(Ksiegowy ksiegowy) {
        entityManager.remove(entityManager.contains(ksiegowy) ? ksiegowy : entityManager.merge(ksiegowy));
    }
}