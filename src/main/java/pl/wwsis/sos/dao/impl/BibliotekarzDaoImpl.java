package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.BibliotekarzDao;
import pl.wwsis.sos.model.Bibliotekarz;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class BibliotekarzDaoImpl implements BibliotekarzDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Bibliotekarz bibliotekarz) {
        entityManager.persist(bibliotekarz);
    }

    @Override
    public Bibliotekarz findById(Integer id) {
        return entityManager.find(Bibliotekarz.class, id);
    }

    @Override
    public List<Bibliotekarz> findAll() {
        return entityManager.createQuery("SELECT a FROM Bibliotekarz a", Bibliotekarz.class).getResultList();
    }

    @Override
    public void update(Bibliotekarz bibliotekarz) {
        entityManager.merge(bibliotekarz);
    }

    @Override
    public void delete(Bibliotekarz bibliotekarz) {
        entityManager.remove(entityManager.contains(bibliotekarz) ? bibliotekarz : entityManager.merge(bibliotekarz));
    }
}