package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.EgzemplarzDao;
import pl.wwsis.sos.model.Egzemplarz;
import pl.wwsis.sos.model.Ksiazka;
import pl.wwsis.sos.model.enums.Stan;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class EgzemplarzDaoImpl implements EgzemplarzDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Egzemplarz egzemplarz) {
        entityManager.persist(egzemplarz);
    }

    @Override
    public Egzemplarz findById(Integer id) {
        return entityManager.find(Egzemplarz.class, id);
    }

    @Override
    public List<Egzemplarz> findAll() {
        return entityManager.createQuery("SELECT a FROM Egzemplarz a", Egzemplarz.class).getResultList();
    }

    @Override
    public void update(Egzemplarz egzemplarz) {
        entityManager.merge(egzemplarz);
    }

    @Override
    public void delete(Egzemplarz egzemplarz) {
        entityManager.remove(entityManager.contains(egzemplarz) ? egzemplarz : entityManager.merge(egzemplarz));
    }

    @Override
    public List<Egzemplarz> showAvailableEgzemplarze() {
        return entityManager.createQuery("SELECT a FROM Egzemplarz a " +
                                         "WHERE a.stan = 'DOSTEPNY'", Egzemplarz.class).getResultList();
    }

    public void dodajKsiazke(Ksiazka ksiazka, String idEgzemplarza) {
        Egzemplarz egzemplarz = new Egzemplarz();
        egzemplarz.setKsiazka(ksiazka);
        egzemplarz.setStan(Stan.DOSTEPNY);
        egzemplarz.setIdEgzemplarza(idEgzemplarza);

        entityManager.persist(egzemplarz);
    }
}