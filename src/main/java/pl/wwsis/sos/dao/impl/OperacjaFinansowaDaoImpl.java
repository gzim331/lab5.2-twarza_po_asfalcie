package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.OperacjaFinansowaDao;
import pl.wwsis.sos.model.OperacjaFinansowa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class OperacjaFinansowaDaoImpl implements OperacjaFinansowaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(OperacjaFinansowa operacjaFinansowa) {
        entityManager.persist(operacjaFinansowa);
    }

    @Override
    public OperacjaFinansowa findById(Integer id) {
        return entityManager.find(OperacjaFinansowa.class, id);
    }

    @Override
    public List<OperacjaFinansowa> findAll() {
        return entityManager.createQuery("SELECT a FROM OperacjaFinansowa a", OperacjaFinansowa.class).getResultList();
    }

    @Override
    public void update(OperacjaFinansowa operacjaFinansowa) {
        entityManager.merge(operacjaFinansowa);
    }

    @Override
    public void delete(OperacjaFinansowa operacjaFinansowa) {
        entityManager.remove(entityManager.contains(operacjaFinansowa) ? operacjaFinansowa : entityManager.merge(operacjaFinansowa));
    }
}