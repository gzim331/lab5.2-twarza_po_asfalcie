package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.LegitymacjaStudentaDao;
import pl.wwsis.sos.model.LegitymacjaStudenta;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public class LegitymacjaStudentaDaoImpl implements LegitymacjaStudentaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(LegitymacjaStudenta legitymacjaStudenta) {
        entityManager.persist(legitymacjaStudenta);
    }

    @Override
    public LegitymacjaStudenta findById(Integer id) {
        return entityManager.find(LegitymacjaStudenta.class, id);
    }

    @Override
    public List<LegitymacjaStudenta> findAll() {
        return entityManager.createQuery("SELECT a FROM LegitymacjaStudenta a", LegitymacjaStudenta.class).getResultList();
    }

    @Override
    public void update(LegitymacjaStudenta legitymacjaStudenta) {
        entityManager.merge(legitymacjaStudenta);
    }

    @Override
    public void delete(LegitymacjaStudenta legitymacjaStudenta) {
        entityManager.remove(entityManager.contains(legitymacjaStudenta) ? legitymacjaStudenta : entityManager.merge(legitymacjaStudenta));
    }

    @Override
    public boolean sprawdzWaznosc(int numerLegitymacji) {
        List<LegitymacjaStudenta> legitymacje = entityManager.createQuery(
                        "SELECT l FROM LegitymacjaStudenta l WHERE l.numerLegitymacji = :numerLegitymacji", LegitymacjaStudenta.class)
                .setParameter("numerLegitymacji", Integer.toString(numerLegitymacji))
                .getResultList();

        if (legitymacje.isEmpty()) {
            return false; // Brak legitymacji o podanym numerze
        }

        LegitymacjaStudenta legitymacja = legitymacje.get(0);

        LocalDate currentDate = LocalDate.now();
        return !legitymacja.getDataWaznosci().isBefore(currentDate);
    }

    @Override
    public void przedluzWaznosc(int numerLegitymacji, int oIleMiesiecy) {
        List<LegitymacjaStudenta> legitymacje = entityManager.createQuery(
                        "SELECT l FROM LegitymacjaStudenta l WHERE l.numerLegitymacji = :numerLegitymacji", LegitymacjaStudenta.class)
                .setParameter("numerLegitymacji", Integer.toString(numerLegitymacji))
                .getResultList();

        if(!legitymacje.isEmpty()) {
            LegitymacjaStudenta legitymacja = legitymacje.get(0);

            LocalDate nowaDataWaznosci = legitymacja.getDataWaznosci().plusMonths(oIleMiesiecy);
            legitymacja.setDataWaznosci(nowaDataWaznosci);

            entityManager.merge(legitymacja);
        }
    }
}