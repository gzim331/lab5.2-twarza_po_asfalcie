package pl.wwsis.sos.dao.impl;

import pl.wwsis.sos.dao.KsiazkaDao;
import pl.wwsis.sos.model.Ksiazka;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class KsiazkaDaoImpl implements KsiazkaDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Ksiazka ksiazka) {
        entityManager.persist(ksiazka);
    }

    @Override
    public Ksiazka findById(Integer id) {
        return entityManager.find(Ksiazka.class, id);
    }

    @Override
    public List<Ksiazka> findAll() {
        return entityManager.createQuery("SELECT a FROM Ksiazka a", Ksiazka.class).getResultList();
    }

    @Override
    public void update(Ksiazka ksiazka) {
        entityManager.merge(ksiazka);
    }

    @Override
    public void delete(Ksiazka ksiazka) {
        entityManager.remove(entityManager.contains(ksiazka) ? ksiazka : entityManager.merge(ksiazka));
    }

    public List<Ksiazka> findBookByTitle(String title) {
        String queryString = "SELECT a FROM Ksiazka a WHERE a.tytul = :tytul";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("tytul", title);

        return query.getResultList();
    }
}