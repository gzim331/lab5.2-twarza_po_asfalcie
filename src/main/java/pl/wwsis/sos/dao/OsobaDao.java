package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Adres;
import pl.wwsis.sos.model.Osoba;

import java.util.List;

public interface OsobaDao {
    void save(Osoba osoba);
    Osoba findById(Integer id);
    List<Osoba> findAll();
    void update(Osoba osoba);
    void delete(Osoba osoba);
    String pobierzImieINazwisko(Integer osobaId);
    String pobierzInfoAdres(Integer osobaId);
    void ustawNowyAdres(Osoba osoba, Adres nowyAdres);
}
