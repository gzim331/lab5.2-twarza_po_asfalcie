package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Przedmiot;
import pl.wwsis.sos.model.enums.KierunekStudiow;

import java.util.List;

public interface PrzedmiotDao {
    void save(Przedmiot przedmiot);
    Przedmiot findById(Integer id);
    List<Przedmiot> findAll();
    List<Przedmiot> wyswietlPrzedmioty(KierunekStudiow kierunekStudiow, Integer semestr);
    List<Przedmiot> wyszukajPrzedmiotyPoNazwie(String nazwa);
    void update(Przedmiot przedmiot);
    void delete(Przedmiot przedmiot);
}
