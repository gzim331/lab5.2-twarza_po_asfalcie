package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Ksiazka;

import java.util.List;

public interface KsiazkaDao {
    void save(Ksiazka ksiazka);
    Ksiazka findById(Integer id);
    List<Ksiazka> findAll();
    void update(Ksiazka ksiazka);
    void delete(Ksiazka ksiazka);
    List<Ksiazka> findBookByTitle(String title);
}
