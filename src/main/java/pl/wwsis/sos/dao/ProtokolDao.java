package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Protokol;

import java.util.List;

public interface ProtokolDao {
    void save(Protokol protokol);
    Protokol findById(Integer id);
    List<Protokol> findAll();
    void update(Protokol protokol);
    void delete(Protokol protokol);
    Protokol getProtokolWedlugNrProtokolu(Integer nrProtokolu);
    void otworzProtokol(Integer nrProtokolu);
    void zamknijProtokol(Integer nrProtokolu);
}
