package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Autor;

import java.util.List;

public interface AutorDao {
    void save(Autor autor);
    Autor findById(Integer id);
    List<Autor> findAll();
    void update(Autor autor);
    void delete(Autor autor);


//    String showDetails(Integer id);

}
