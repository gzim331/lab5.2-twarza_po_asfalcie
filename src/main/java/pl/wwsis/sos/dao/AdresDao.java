package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Adres;

import java.util.List;

public interface AdresDao {
    void save(Adres adres);
    Adres findById(Integer id);
    List<Adres> findAll();
    void update(Adres adres);
    void delete(Adres adres);
    String showDetails(Integer id);
}
