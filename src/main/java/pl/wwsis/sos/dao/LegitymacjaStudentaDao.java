package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.LegitymacjaStudenta;

import java.util.List;

public interface LegitymacjaStudentaDao {
    void save(LegitymacjaStudenta legitymacjaStudenta);
    LegitymacjaStudenta findById(Integer id);
    List<LegitymacjaStudenta> findAll();
    void update(LegitymacjaStudenta legitymacjaStudenta);
    void delete(LegitymacjaStudenta legitymacjaStudenta);
    boolean sprawdzWaznosc(int numerLegitymacji);
    void przedluzWaznosc(int numerLegitymacji, int oIleMiesiecy);
}
