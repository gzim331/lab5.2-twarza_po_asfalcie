package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Wykladowca;

import java.util.List;

public interface WykladowcaDao {
    void save(Wykladowca wykladowca);
    Wykladowca findById(Integer id);
    List<Wykladowca> findAll();
    void update(Wykladowca wykladowca);
    void delete(Wykladowca wykladowca);

/*    //Niepotrzebna metoda:
    String showAllWykladowca();*/
}
