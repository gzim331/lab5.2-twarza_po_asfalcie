package pl.wwsis.sos.dao;

import pl.wwsis.sos.model.Przedmiot;
import pl.wwsis.sos.model.Student;
import pl.wwsis.sos.model.StudentPrzedmiot;

import javax.transaction.Transactional;
import java.util.List;

public interface StudentPrzedmiotDao {
    void save(StudentPrzedmiot studentPrzedmiot);
    StudentPrzedmiot findById(Integer id);
    List<StudentPrzedmiot> findAll();
    void update(StudentPrzedmiot studentPrzedmiot);
    void delete(StudentPrzedmiot studentPrzedmiot);
    void wypiszStudenta(Student student, Przedmiot przedmiot);
    Boolean czyStudentJestWPrzedmiocie(Student student, Przedmiot przedmiot);
}
