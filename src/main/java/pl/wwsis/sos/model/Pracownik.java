package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "pracownik")
public class Pracownik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "stanowisko", length = 50, nullable = false)
    private String stanowisko;

    @Column(name = "data_zatrudnienia", nullable = false)
    private LocalDate dataZatrudnienia;

    @Column(name = "data_zakonczenia_pracy")
    private LocalDate dataZakonczeniaPracy;

    @ManyToOne
    @JoinColumn(name = "osoba_id", nullable = false)
    private Osoba osoba;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    public void setDataZatrudnienia(LocalDate dataZatrudnienia) {
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public LocalDate getDataZakonczeniaPracy() {
        return dataZakonczeniaPracy;
    }

    public void setDataZakonczeniaPracy(LocalDate dataZakonczeniaPracy) {
        this.dataZakonczeniaPracy = dataZakonczeniaPracy;
    }

    public Osoba getOsoba() {
        return osoba;
    }

    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
    }
}
