package pl.wwsis.sos.model;

import pl.wwsis.sos.model.enums.KierunekStudiow;
import pl.wwsis.sos.model.enums.TrybStudiow;

import javax.persistence.*;

@Entity
@Table(name = "przedmiot")
public class Przedmiot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nazwa_przedmiotu", length = 200)
    private String nazwaPrzedmiotu;

    @Column(name = "punkty_ects")
    private Integer punktyEcts;

    @Column(name = "liczba_godzin", nullable = false)
    private Integer liczbaGodzin;

    @Column(name = "jezyk_wykladowy", length = 2, nullable = false)
    private String jezykWykladowy;

    @Column(name = "opis", length = 200, nullable = false)
    private String opis;

    @Column(name = "semestr", nullable = false)
    private Integer semestr;

    @Enumerated(EnumType.STRING)
    @Column(name = "kierunek_studiow", nullable = false)
    private KierunekStudiow kierunekStudiow;

    @Enumerated(EnumType.STRING)
    @Column(name = "tryb_studiow", nullable = false)
    private TrybStudiow trybStudiow = TrybStudiow.STACJONARNE;

    @ManyToOne
    @JoinColumn(name = "wykladowca_id")
    private Wykladowca wykladowca;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNazwaPrzedmiotu() {
        return nazwaPrzedmiotu;
    }

    public void setNazwaPrzedmiotu(String nazwaPrzedmiotu) {
        this.nazwaPrzedmiotu = nazwaPrzedmiotu;
    }

    public Integer getPunktyEcts() {
        return punktyEcts;
    }

    public void setPunktyEcts(Integer punktyEcts) {
        this.punktyEcts = punktyEcts;
    }

    public Integer getLiczbaGodzin() {
        return liczbaGodzin;
    }

    public void setLiczbaGodzin(Integer liczbaGodzin) {
        this.liczbaGodzin = liczbaGodzin;
    }

    public String getJezykWykladowy() {
        return jezykWykladowy;
    }

    public void setJezykWykladowy(String jezykWykladowy) {
        this.jezykWykladowy = jezykWykladowy;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }

    public KierunekStudiow getKierunekStudiow() {
        return kierunekStudiow;
    }

    public void setKierunekStudiow(KierunekStudiow kierunekStudiow) {
        this.kierunekStudiow = kierunekStudiow;
    }

    public TrybStudiow getTrybStudiow() {
        return trybStudiow;
    }

    public void setTrybStudiow(TrybStudiow trybStudiow) {
        this.trybStudiow = trybStudiow;
    }

    public Wykladowca getWykladowca() {
        return wykladowca;
    }

    public void setWykladowca(Wykladowca wykladowca) {
        this.wykladowca = wykladowca;
    }
}
