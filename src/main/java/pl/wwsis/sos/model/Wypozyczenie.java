package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "wypozyczenie")
public class Wypozyczenie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "data_wypozyczenia")
    private LocalDate dataWypozyczenia;

    @Column(name = "data_rezerwacji")
    private LocalDate dataRezerwacji;

    @Column(name = "data_oddania")
    private LocalDate dataOddania;

    @Column(name = "termin_oddania")
    private LocalDate terminOddania;

    @ManyToOne
    @JoinColumn(name = "bibliotekarz_id")
    private Bibliotekarz bibliotekarz;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "egzemplarz_id")
    private Egzemplarz egzemplarz;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDataWypozyczenia() {
        return dataWypozyczenia;
    }

    public void setDataWypozyczenia(LocalDate dataWypozyczenia) {
        this.dataWypozyczenia = dataWypozyczenia;
    }

    public LocalDate getDataRezerwacji() {
        return dataRezerwacji;
    }

    public void setDataRezerwacji(LocalDate dataRezerwacji) {
        this.dataRezerwacji = dataRezerwacji;
    }

    public LocalDate getDataOddania() {
        return dataOddania;
    }

    public void setDataOddania(LocalDate dataOddania) {
        this.dataOddania = dataOddania;
    }

    public LocalDate getTerminOddania() {
        return terminOddania;
    }

    public void setTerminOddania(LocalDate terminOddania) {
        this.terminOddania = terminOddania;
    }

    public Bibliotekarz getBibliotekarz() {
        return bibliotekarz;
    }

    public void setBibliotekarz(Bibliotekarz bibliotekarz) {
        this.bibliotekarz = bibliotekarz;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Egzemplarz getEgzemplarz() {
        return egzemplarz;
    }

    public void setEgzemplarz(Egzemplarz egzemplarz) {
        this.egzemplarz = egzemplarz;
    }
}
