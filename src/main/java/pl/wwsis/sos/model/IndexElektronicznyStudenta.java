package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "indeks_elektroniczny_studenta")
public class IndexElektronicznyStudenta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nr_indeksu", nullable = false)
    private Integer nrIndeksu;

    @Column(name = "data_zalozenia", nullable = false)
    private LocalDate dataZalozenia;

    @Column(name = "temat_pracy_dyplomowej", length = 50)
    private String tematPracyDyplomowej;

    @Column(name = "zdjecie", length = 200)
    private String zdjecie;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(Integer nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public LocalDate getDataZalozenia() {
        return dataZalozenia;
    }

    public void setDataZalozenia(LocalDate dataZalozenia) {
        this.dataZalozenia = dataZalozenia;
    }

    public String getTematPracyDyplomowej() {
        return tematPracyDyplomowej;
    }

    public void setTematPracyDyplomowej(String tematPracyDyplomowej) {
        this.tematPracyDyplomowej = tematPracyDyplomowej;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
