package pl.wwsis.sos.model;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.time.LocalDate;

@Entity
@Table(name = "protokol")
public class Protokol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nr_protokolu", nullable = false)
    private Integer nrProtokolu;

    @Column(name = "ocena", nullable = false)
    private Double ocena;

    @Column(name = "status", nullable = false)
    private Boolean status;

    @Column(name = "data_zamkniecia")
    private LocalDate dataZamkniecia;

    @Column(name = "data_oceny")
    private LocalDate dataOceny;

    @ManyToOne
    @JoinColumn(name = "przedmiot_id")
    private Przedmiot przedmiot;

    @ManyToOne
    @JoinColumn(name = "wykladowca_id")
    private Wykladowca wykladowca;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNrProtokolu() {
        return nrProtokolu;
    }

    public void setNrProtokolu(Integer nrProtokolu) {
        this.nrProtokolu = nrProtokolu;
    }

    public Double getOcena() {
        return ocena;
    }

    public void setOcena(Double ocena) {
        this.ocena = ocena;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getDataZamkniecia() {
        return dataZamkniecia;
    }

    public void setDataZamkniecia(LocalDate dataZamkniecia) {
        this.dataZamkniecia = dataZamkniecia;
    }

    public LocalDate getDataOceny() {
        return dataOceny;
    }

    public void setDataOceny(LocalDate dataOceny) {
        this.dataOceny = dataOceny;
    }

    public Przedmiot getPrzedmiot() {
        return przedmiot;
    }

    public void setPrzedmiot(Przedmiot przedmiot) {
        this.przedmiot = przedmiot;
    }

    public Wykladowca getWykladowca() {
        return wykladowca;
    }

    public void setWykladowca(Wykladowca wykladowca) {
        this.wykladowca = wykladowca;
    }
}
