package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "legitymacja_studenta")
public class LegitymacjaStudenta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "numer_legitymacji", length = 50, nullable = false)
    private String numerLegitymacji;

    @Column(name = "data_waznosci", nullable = false)
    private LocalDate dataWaznosci;

    @Column(name = "zdjecie", length = 250, nullable = false)
    private String zdjecie;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumerLegitymacji() {
        return numerLegitymacji;
    }

    public void setNumerLegitymacji(String numerLegitymacji) {
        this.numerLegitymacji = numerLegitymacji;
    }

    public LocalDate getDataWaznosci() {
        return dataWaznosci;
    }

    public void setDataWaznosci(LocalDate dataWaznosci) {
        this.dataWaznosci = dataWaznosci;
    }

    public String getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
