package pl.wwsis.sos.model;

import pl.wwsis.sos.model.enums.KierunekStudiow;
import pl.wwsis.sos.model.enums.Ranga;

import javax.persistence.*;

@Entity
@Table(name = "grupa")
public class Grupa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "nazwa_kierunku", nullable = false)
    private KierunekStudiow nazwaKierunku;

    @Column(name = "semestr", nullable = false)
    private Integer semestr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public KierunekStudiow getNazwaKierunku() {
        return nazwaKierunku;
    }

    public void setNazwaKierunku(KierunekStudiow nazwaKierunku) {
        this.nazwaKierunku = nazwaKierunku;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }
}
