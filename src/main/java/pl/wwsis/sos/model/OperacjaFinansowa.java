package pl.wwsis.sos.model;

import pl.wwsis.sos.model.enums.KierunekStudiow;
import pl.wwsis.sos.model.enums.KierunekTransakcji;
import pl.wwsis.sos.model.enums.TypTransakcji;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.time.LocalDate;

@Entity
@Table(name = "operacja_finansowa")
public class OperacjaFinansowa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "utworzono", nullable = false)
    private LocalDate utworzono;

    @Column(name = "kwota", nullable = false)
    private DecimalFormat kwota;

    @Enumerated(EnumType.STRING)
    @Column(name = "kierunek", nullable = false)
    private KierunekTransakcji kierunek;

    @Column(name = "tytuł", length = 100, nullable = false)
    private String tytul;

    @Enumerated(EnumType.STRING)
    @Column(name = "typ_transakcji", nullable = false)
    private TypTransakcji typTransakcji;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getUtworzono() {
        return utworzono;
    }

    public void setUtworzono(LocalDate utworzono) {
        this.utworzono = utworzono;
    }

    public DecimalFormat getKwota() {
        return kwota;
    }

    public void setKwota(DecimalFormat kwota) {
        this.kwota = kwota;
    }

    public KierunekTransakcji getKierunek() {
        return kierunek;
    }

    public void setKierunek(KierunekTransakcji kierunek) {
        this.kierunek = kierunek;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public TypTransakcji getTypTransakcji() {
        return typTransakcji;
    }

    public void setTypTransakcji(TypTransakcji typTransakcji) {
        this.typTransakcji = typTransakcji;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
