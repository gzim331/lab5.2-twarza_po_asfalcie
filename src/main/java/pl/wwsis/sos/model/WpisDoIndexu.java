package pl.wwsis.sos.model;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.time.LocalDate;

@Entity
@Table(name = "wpis_do_indeksu")
public class WpisDoIndexu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ocena")
    private DecimalFormat ocena;

    @Column(name = "data_wpis")
    private LocalDate dataWpis;

    @ManyToOne
    @JoinColumn(name = "protokol_id")
    private Protokol protokol;

    @ManyToOne
    @JoinColumn(name = "indeks_elektroniczny_studenta_id")
    private IndexElektronicznyStudenta indexElektronicznyStudenta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DecimalFormat getOcena() {
        return ocena;
    }

    public void setOcena(DecimalFormat ocena) {
        this.ocena = ocena;
    }

    public LocalDate getDataWpis() {
        return dataWpis;
    }

    public void setDataWpis(LocalDate dataWpis) {
        this.dataWpis = dataWpis;
    }

    public Protokol getProtokol() {
        return protokol;
    }

    public void setProtokol(Protokol protokol) {
        this.protokol = protokol;
    }

    public IndexElektronicznyStudenta getIndexElektronicznyStudenta() {
        return indexElektronicznyStudenta;
    }

    public void setIndexElektronicznyStudenta(IndexElektronicznyStudenta indexElektronicznyStudenta) {
        this.indexElektronicznyStudenta = indexElektronicznyStudenta;
    }
}
