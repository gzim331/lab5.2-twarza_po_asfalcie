package pl.wwsis.sos.model;

import pl.wwsis.sos.model.enums.Stan;

import javax.persistence.*;

@Entity
@Table(name = "egzemplarz")
public class Egzemplarz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_egzemplarza", length = 100)
    private String idEgzemplarza;

    @Enumerated(EnumType.STRING)
    @Column(name = "stan")
    private Stan stan;

    @ManyToOne
    @JoinColumn(name = "ksiazka_id")
    private Ksiazka ksiazka;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdEgzemplarza() {
        return idEgzemplarza;
    }

    public void setIdEgzemplarza(String idEgzemplarza) {
        this.idEgzemplarza = idEgzemplarza;
    }

    public Stan getStan() {
        return stan;
    }

    public void setStan(Stan stan) {
        this.stan = stan;
    }

    public Ksiazka getKsiazka() {
        return ksiazka;
    }

    public void setKsiazka(Ksiazka ksiazka) {
        this.ksiazka = ksiazka;
    }
}
