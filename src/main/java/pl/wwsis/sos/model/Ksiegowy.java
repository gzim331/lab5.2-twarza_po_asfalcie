package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ksiegowy")
public class Ksiegowy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "dzial", length = 50)
    private String dzial;

    @ManyToOne
    @JoinColumn(name = "pracownik_id", nullable = false)
    private Pracownik pracownik;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDzial() {
        return dzial;
    }

    public void setDzial(String dzial) {
        this.dzial = dzial;
    }

    public Pracownik getPracownik() {
        return pracownik;
    }

    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }
}
