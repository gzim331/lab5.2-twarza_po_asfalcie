package pl.wwsis.sos.model;

import pl.wwsis.sos.model.enums.Ranga;

import javax.persistence.*;

@Entity
@Table(name = "bibliotekarz")
public class Bibliotekarz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ranga", nullable = false)
    private Ranga ranga;

    @ManyToOne
    @JoinColumn(name = "pracownik_id")
    private Pracownik pracownik;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Ranga getRanga() {
        return ranga;
    }

    public void setRanga(Ranga ranga) {
        this.ranga = ranga;
    }

    public Pracownik getPracownik() {
        return pracownik;
    }

    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }
}
