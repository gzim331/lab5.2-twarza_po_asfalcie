package pl.wwsis.sos.model;

import pl.wwsis.sos.model.enums.StopienNaukowy;

import javax.persistence.*;

@Entity
@Table(name = "wykladowca")
public class Wykladowca {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "specjalizacja", length = 200, nullable = false)
    private String specjalizacja;

    @Column(name = "doswiadczenie", length = 200, nullable = false)
    private String doswiadczenie;

    @Enumerated(EnumType.STRING)
    @Column(name = "stopien_naukowy", nullable = false)
    private StopienNaukowy stopienNaukowy = StopienNaukowy.MAGISTER;

    @ManyToOne
    @JoinColumn(name = "pracownik_id")
    private Pracownik pracownik;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpecjalizacja() {
        return specjalizacja;
    }

    public void setSpecjalizacja(String specjalizacja) {
        this.specjalizacja = specjalizacja;
    }

    public String getDoswiadczenie() {
        return doswiadczenie;
    }

    public void setDoswiadczenie(String doswiadczenie) {
        this.doswiadczenie = doswiadczenie;
    }

    public StopienNaukowy getStopienNaukowy() {
        return stopienNaukowy;
    }

    public void setStopienNaukowy(StopienNaukowy stopienNaukowy) {
        this.stopienNaukowy = stopienNaukowy;
    }

    public Pracownik getPracownik() {
        return pracownik;
    }

    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }
}
