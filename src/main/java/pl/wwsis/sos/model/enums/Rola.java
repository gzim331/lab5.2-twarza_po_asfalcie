package pl.wwsis.sos.model.enums;

public enum Rola {
    STUDENT,
    WYKLADOWCA,
    BIBLIOTEKARZ,
    DZIEKANAT,
    KSIEGOWY
}
