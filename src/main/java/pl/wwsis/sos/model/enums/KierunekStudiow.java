package pl.wwsis.sos.model.enums;

public enum KierunekStudiow {
    INFORMATYKA,
    AUTOMATYKA_I_ROBOTYKA,
    MECHATRONIKA
}
