package pl.wwsis.sos.model.enums;

public enum TypTransakcji {
    CZESNE,
    STYPENDIUM,
    OPLATA_ZA_PROMOTORA
}
