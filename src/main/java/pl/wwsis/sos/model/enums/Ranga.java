package pl.wwsis.sos.model.enums;

public enum Ranga {
    PRAKTYKANT,
    KIEROWNIK,
    STARSZY_BIBLIOTEKARZ
}
