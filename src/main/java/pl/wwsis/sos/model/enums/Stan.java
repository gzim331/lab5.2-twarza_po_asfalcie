package pl.wwsis.sos.model.enums;

public enum Stan {
    WYPOZYCZONY,
    ZAREZERWOWANY,
    DOSTEPNY,
    NOWY
}
