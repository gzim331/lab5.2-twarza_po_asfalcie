package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "data_rozpoczęcia_studiow", nullable = false)
    private LocalDate dataRozpoczeciaStudiow;

    @Column(name = "tok_indywidualny", nullable = false)
    private Boolean tokIndywidualny = false;

    @Column(name = "numer_umowy", length = 50, nullable = false)
    private String numerUmowy;

    @Column(name = "status", length = 50, nullable = false)
    private String status;

    @ManyToOne
    @JoinColumn(name = "osoba_id")
    private Osoba osoba;

    @ManyToOne
    @JoinColumn(name = "grupa_id")
    private Grupa grupa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDataRozpoczeciaStudiow() {
        return dataRozpoczeciaStudiow;
    }

    public void setDataRozpoczeciaStudiow(LocalDate dataRozpoczeciaStudiow) {
        this.dataRozpoczeciaStudiow = dataRozpoczeciaStudiow;
    }

    public Boolean getTokIndywidualny() {
        return tokIndywidualny;
    }

    public void setTokIndywidualny(Boolean tokIndywidualny) {
        this.tokIndywidualny = tokIndywidualny;
    }

    public String getNumerUmowy() {
        return numerUmowy;
    }

    public void setNumerUmowy(String numerUmowy) {
        this.numerUmowy = numerUmowy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Osoba getOsoba() {
        return osoba;
    }

    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
    }

    public Grupa getGrupa() {
        return grupa;
    }

    public void setGrupa(Grupa grupa) {
        this.grupa = grupa;
    }
}
