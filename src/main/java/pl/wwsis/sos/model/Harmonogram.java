package pl.wwsis.sos.model;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;

@Entity
@Table(name = "harmonogram")
public class Harmonogram {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "data_zajec", nullable = false)
    private LocalDate dataZajec;

    @Column(name = "start_zajec", nullable = false)
    private Time startZajec;

    @Column(name = "koniec_zajec", nullable = false)
    private Time koniecZajec;

    @ManyToOne
    @JoinColumn(name = "grupa_id")
    private Grupa grupa;

    @ManyToOne
    @JoinColumn(name = "przedmiot_id")
    private Przedmiot przedmiot;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDataZajec() {
        return dataZajec;
    }

    public void setDataZajec(LocalDate dataZajec) {
        this.dataZajec = dataZajec;
    }

    public Time getStartZajec() {
        return startZajec;
    }

    public void setStartZajec(Time startZajec) {
        this.startZajec = startZajec;
    }

    public Time getKoniecZajec() {
        return koniecZajec;
    }

    public void setKoniecZajec(Time koniecZajec) {
        this.koniecZajec = koniecZajec;
    }

    public Grupa getGrupa() {
        return grupa;
    }

    public void setGrupa(Grupa grupa) {
        this.grupa = grupa;
    }

    public Przedmiot getPrzedmiot() {
        return przedmiot;
    }

    public void setPrzedmiot(Przedmiot przedmiot) {
        this.przedmiot = przedmiot;
    }
}
