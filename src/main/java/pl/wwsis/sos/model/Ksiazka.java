package pl.wwsis.sos.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ksiazka")
public class Ksiazka {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "tytul", length = 100, nullable = false)
    private String tytul;

    @Column(name = "ilosc_stron")
    private Integer iloscStron;

    @Column(name = "data_wydania")
    private LocalDate dataWydania;

    @Column(name = "wydawnictwo", length = 250)
    private String wydawnictwo;

    @Column(name = "wydanie")
    private Integer wydanie;

    @Column(name = "kategoria", length = 100)
    private String kategoria;

    @ManyToOne
    @JoinColumn(name = "autor_id")
    private Autor autor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public Integer getIloscStron() {
        return iloscStron;
    }

    public void setIloscStron(Integer iloscStron) {
        this.iloscStron = iloscStron;
    }

    public LocalDate getDataWydania() {
        return dataWydania;
    }

    public void setDataWydania(LocalDate dataWydania) {
        this.dataWydania = dataWydania;
    }

    public String getWydawnictwo() {
        return wydawnictwo;
    }

    public void setWydawnictwo(String wydawnictwo) {
        this.wydawnictwo = wydawnictwo;
    }

    public Integer getWydanie() {
        return wydanie;
    }

    public void setWydanie(Integer wydanie) {
        this.wydanie = wydanie;
    }

    public String getKategoria() {
        return kategoria;
    }

    public void setKategoria(String kategoria) {
        this.kategoria = kategoria;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }
}
