package pl.wwsis.sos.model;


import pl.wwsis.sos.model.enums.Rola;

import javax.persistence.*;

@Entity
@Table(name = "konto")
public class Konto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "login", length = 100, nullable = false)
    private String login;

    @Column(name = "haslo", length = 100, nullable = false)
    private String haslo;

    @Column(name = "status", nullable = false)
    private Boolean status = true;

    @Enumerated(EnumType.STRING)
    @Column(name = "rola", nullable = false)
    private Rola rola;

    @ManyToOne
    @JoinColumn(name = "osoba_id")
    private Osoba osoba;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Rola getRola() {
        return rola;
    }

    public void setRola(Rola rola) {
        this.rola = rola;
    }

    public Osoba getOsoba() {
        return osoba;
    }

    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
    }
}
