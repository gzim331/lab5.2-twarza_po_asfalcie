package pl.wwsis.sos.dao.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.wwsis.sos.model.Przedmiot;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PrzedmiotDaoImplTest {

    @Mock
    private EntityManager entityManager;

    @Mock
    private TypedQuery<Przedmiot> typedQuery;

    @InjectMocks
    private PrzedmiotDaoImpl przedmiotDao;
    private Przedmiot przedmiot;


    @BeforeEach
    public void setUp() {
        przedmiot = new Przedmiot();
        przedmiot.setId(1);
        przedmiot.setNazwaPrzedmiotu("Mein Kampf");
        przedmiot.setSemestr(4);
    }

    @Test
    public void testSave() {
        przedmiotDao.save(przedmiot);
        verify(entityManager, times(1)).persist(przedmiot);
    }

    @Test
    public void testFindById() {
        when(entityManager.find(Przedmiot.class, 1)).thenReturn(przedmiot);
        Przedmiot found = przedmiotDao.findById(1);
        assertEquals(przedmiot, found);
    }

    @Test
    public void testUpdate() {
        przedmiot.setNazwaPrzedmiotu("Kampania Zimki");
        przedmiotDao.update(przedmiot);
        verify(entityManager, times(1)).merge(przedmiot);
    }

    @Test
    public void testDelete() {
        when(entityManager.contains(przedmiot)).thenReturn(true);
        przedmiotDao.delete(przedmiot);
        verify(entityManager, times(1)).remove(przedmiot);
    }
}
