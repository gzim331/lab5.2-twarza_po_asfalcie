package pl.wwsis.sos.dao.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.wwsis.sos.model.Osoba;
import pl.wwsis.sos.model.Pracownik;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PracownikDaoImplTest {

    @Mock
    private EntityManager entityManager;

    @Mock
    private TypedQuery<Pracownik> typedQuery;

    @InjectMocks
    private PracownikDaoImpl pracownikDao;

    private Pracownik pracownik;
    private Osoba osoba;

    @BeforeEach
    public void setUp() {
        osoba = new Osoba();
        osoba.setId(1);
        osoba.setImie("Jan");
        osoba.setNazwisko("Kowalski");

        pracownik = new Pracownik();
        pracownik.setId(1);
        pracownik.setStanowisko("Programista");
        pracownik.setDataZatrudnienia(LocalDate.now());
        pracownik.setOsoba(osoba);
    }

    @Test
    public void testSave() {
        pracownikDao.save(pracownik);
        verify(entityManager, times(1)).persist(pracownik);
    }

    @Test
    public void testFindById() {
        when(entityManager.find(Pracownik.class, 1)).thenReturn(pracownik);
        Pracownik found = pracownikDao.findById(1);
        assertEquals(pracownik, found);
    }

    @Test
    public void testFindAll() {
        List<Pracownik> pracownicy = Arrays.asList(pracownik);
        when(entityManager.createQuery("SELECT a FROM Pracownik a", Pracownik.class)).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(pracownicy);

        List<Pracownik> result = pracownikDao.findAll();
        assertEquals(pracownicy, result);
    }

    @Test
    public void testUpdate() {
        pracownikDao.update(pracownik);
        verify(entityManager, times(1)).merge(pracownik);
    }

    @Test
    public void testDelete() {
        when(entityManager.contains(pracownik)).thenReturn(true);
        pracownikDao.delete(pracownik);
        verify(entityManager, times(1)).remove(pracownik);
    }

    @Test
    public void testDelete_NotContained() {
        when(entityManager.contains(pracownik)).thenReturn(false);
        when(entityManager.merge(pracownik)).thenReturn(pracownik);
        pracownikDao.delete(pracownik);
        verify(entityManager, times(1)).remove(pracownik);
    }

    @Test
    public void testWyszukajPracownika() {
        List<Pracownik> pracownicy = Arrays.asList(pracownik);
        when(entityManager.createQuery(anyString(), eq(Pracownik.class))).thenReturn(typedQuery);
        when(typedQuery.setParameter(anyString(), anyString())).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(pracownicy);

        List<Pracownik> result = pracownikDao.wyszukajPracownika("Jan", "Kowalski");
        assertEquals(pracownicy, result);
    }
}
